from collections import defaultdict
from Classes.FirestoreHandler import FirestoreHandler
from DialogueSystem.NameEntityDetector import NameEntityDetector
import spacy

basepath = r"/Users/plg519/WebAI/data_and_models"
cred_path = basepath + "/tutorsdb-f22d1-7d01c499c3af.json"
firestore_handler = FirestoreHandler(cred_path)

def build_cate_dict(attributes, firestore_handler):
    cate_attribute_values_dict = defaultdict(list)
    for attribute in attributes:
        # print('attribute: ', attribute)
        # values = firestore_handler.get_all_values_by_attribute('tutors', attribute)
        # firestore_handler.refresh_attribute_collection(u'tutors', u'subjects', u'tutor_attributes')
        # all_sub_docs = firestore_handler.get_document(u'tutor_attributes', u'tutors_subjects')
        all_sub_docs = firestore_handler.get_document(u'tutor_attributes', u'tutors_' + attribute)
        # print("all_sub_docs: \n", all_sub_docs[0]['data'])
        values = all_sub_docs[0]['data']
        # lower_case_values = [s.lower() for s in values]
        # cate_attribute_values_dict[attribute].extend(lower_case_values)
        cate_attribute_values_dict[attribute].extend(values)
    return cate_attribute_values_dict




categorical_attributes = set(['subjects', 'locations','educations'])
# categorical_attributes = set(['subjects','educations'])
continuous_attributes = set(['tutoring_experience', 'acceptable_hourly_rate'])

cate_attribute_values_dict = build_cate_dict(categorical_attributes, firestore_handler)
# print("cate_attribute_values_dict: ", cate_attribute_values_dict)

exp_keywords = ['years experience']
rate_keywords = ['$', 'dollar', 'dollars']

continuous_attribute_values_dict = defaultdict(list)
continuous_attribute_values_dict['tutoring_experience'].extend(exp_keywords)
continuous_attribute_values_dict['acceptable_hourly_rate'].extend(rate_keywords)

# print("continuous_attribute_values_dict: ", continuous_attribute_values_dict)

nlp = spacy.load('en_core_web_sm')
# doc = nlp(u'I need a professional piano tutor with at least 15 years experience in Plano area. better graduated from harvard,'
#           u'with rate less than 30 dollars per hour')

# doc = nlp(u'I need a professional science tutor with 3 years experience in Plano area, better graduate from Princeton University '
#           u'with rate less than 10 dollars per hour')







#Demo 1
# doc = nlp(u'can you find me a piano tutor?')

# doc = nlp(u'can you find me a piano tutor, with rate less than 90 dollars per hour?')

# doc = nlp(u'I need a geometry tutor who graduated from Stanford , with rate less than 80 dollars per hour')


#Demo 2: for fuzzy match
# doc = nlp(u'can you find me a alchemy tutor?')


#Demo3: for location only
doc = nlp(u'can you find some tutor in los angeles, ca?')


#Demo 4: for adding location
# doc = nlp(u'can you find me a piano tutor in new york, ny?')













#Demo 3

# doc = nlp(u'i want a physics teacher, with rate less than 50 dollars') #benpickles Hourly Rate: $25 not found when searching less than $50
# doc = nlp(u'i want a physics teacher, with rate less than 50.5 dollars')
#Traceback (most recent call last):
 # File "/Users/plg519/WebAI/Test/test_NameEntityDetector.py", line 131, in <module>
 #   continuous_matching_ids = firestore_handler.get_multi_documents_ids(u'tutors', attribute, operator_str, int(value[0]))
#ValueError: invalid literal for int() with base 10: '50.5'















ner = NameEntityDetector(cate_attribute_values_dict, continuous_attribute_values_dict)

synnonym_origin_dict = ner.build_synonym_word_dict()
# for synonym, origin in synnonym_origin_dict.items():
#     print(synonym, " from ", origin)
# print("synnonym_origin_dict: ", synnonym_origin_dict)

# categorical_entity_word_dict, continuous_entity_word_dict = ner.build_attribute_word_dicts(doc)
categorical_entity_word_dict, continuous_entity_word_dict = ner.build_attribute_word_dicts_with_synonyms(doc, synnonym_origin_dict)

print("entity_categorical_entity_word_dict: \n", categorical_entity_word_dict)
# print("continuous_entity_word_dict: \n", continuous_entity_word_dict)







# # query by subjects

overlapping_sets = []

# categorical_matching_ids = firestore_handler.get_multi_documents(u'tutors', u'subjects', u'array_contains', u'Piano')
# print("categorical_matching_ids: ", categorical_matching_ids)


for attribute, value in categorical_entity_word_dict.items():
    # print("attribute: ", attribute, " value: ", value[0])
    # firestore_handler.get_multi_documents(u'tutors', u'subjects', u'array_contains', u'English')
    # print("value[0].capitalize(): ", value[0].capitalize())
    # break
    # categorical_matching_docs = firestore_handler.get_multi_documents(u'tutors', attribute, u'array_contains', value[0].capitalize())
    # categorical_matching_ids = firestore_handler.get_multi_documents_ids(u'tutors', attribute, u'array_contains', value[0].capitalize())
    if attribute == 'subjects':
        categorical_matching_ids = firestore_handler.get_multi_documents_ids(u'tutors', attribute, u'array_contains', value[0])
    if attribute == 'locations':
        categorical_matching_ids = firestore_handler.get_multi_documents_ids(u'tutors_w_locations', 'location', u'==', ' ' + value[0])


    # print("categorical_matching_ids: ", categorical_matching_ids)
    # firestore_handler.get_multi_documents(u'tutors', attribute, u'array_contains', value[0])
    # need to save all subject values in db as lower cases, so query can find it
    matching_ids_set = set(categorical_matching_ids)
    # print("matching_ids_set: ", matching_ids_set)
    overlapping_sets.append(matching_ids_set)


for attribute, value in continuous_entity_word_dict.items():
    # print("attribute: ", attribute, " value: ", value[0])
    # firestore_handler.get_multi_documents(u'tutors', u'subjects', u'array_contains', u'English')
    if attribute == "tutoring_experience":
        operator_str = ">"
    if attribute == "acceptable_hourly_rate":
        operator_str = "<"
    continuous_matching_ids = firestore_handler.get_multi_documents_ids(u'tutors', attribute, operator_str, int(value[0]))
    # print("continuous_matching_ids: ", continuous_matching_ids)
    # need to save all subject values in db as lower cases, so query can find it
    #add applying multiple rules filters sequentially and get overlapping tutors, use doc.id to get intersection
    matching_ids_set = set(continuous_matching_ids)
    overlapping_sets.append(matching_ids_set)

# print("overlapping_sets: ", overlapping_sets)

overlapping_ids = set.intersection(*overlapping_sets)
print("matching_ids: ", overlapping_ids)
