# Install chromedriver from https://sites.google.com/a/chromium.org/chromedriver/downloads

import os

from optparse import OptionParser

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

# CHROME_PATH = '/usr/bin/google-chrome'
CHROME_PATH = '/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome'

# CHROMEDRIVER_PATH = '/usr/bin/chromedriver'
CHROMEDRIVER_PATH = '/Users/plg519/ChromeDriver/chromedriver'

# WINDOW_SIZE = "1920,1080"
WINDOW_SIZE = "1440,800"


chrome_options = Options()
chrome_options.add_argument("--headless")
# chrome_options.add_argument("--window-size=%s" % WINDOW_SIZE)
# chrome_options.binary_location = CHROME_PATH

def make_screenshot(url, output):
    if not url.startswith('http'):
        raise Exception('URLs need to start with "http"')

    driver = webdriver.Chrome(
        executable_path=CHROMEDRIVER_PATH,
        chrome_options=chrome_options
    )
    # driver = webdriver.Chrome(
    #     executable_path=CHROMEDRIVER_PATH
    # )
    driver.get(url)
    driver.save_screenshot(output)
    driver.close()

if __name__ == '__main__':
    usage = "usage: %prog [options] <url> <output>"
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()

    if len(args) < 2:
        parser.error("please specify a URL and an output")

    make_screenshot(args[0], args[1])
