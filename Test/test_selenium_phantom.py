from selenium import webdriver
from os import getcwd
from os import path

PHANTOMJS_EXECUTABLE_PATH = r"/Users/plg519/node_modules/phantomjs/lib/phantom/bin/phantomjs"

def save_screenshot(url, save_path = path.join(getcwd(), "test.png")):
    driver = webdriver.PhantomJS(PHANTOMJS_EXECUTABLE_PATH)
    driver.get(url)
    driver.save_screenshot(save_path)


# url = r"http://www.yahoo.com"
url = r"https://www.acetutor.io/prahladmenon"

save_screenshot(url)