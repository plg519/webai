import pyautogui
from pyautogui import tweens
import time
import cv2
import numpy as np
from pytesseract import image_to_string

time.sleep(5)

# for i in range(5):
#     pyautogui.scroll(-10, pause=1.)


x = 1440
y = 900
screenshots = []

while True:
    screenshots.append(pyautogui.screenshot())
    if len(screenshots) > 1 and screenshots[-1] == screenshots[-2]:
        break
    pyautogui.scroll(-15, pause=1.)