import cv2
from Classes.ColorImage import ColorImage
from pytesseract import image_to_string


email_im = cv2.imread("email.png")

test_im = ColorImage(pixel_values=email_im)

resized_im = test_im.resize(height=900)
resized_im = resized_im._pixel_values

gray = cv2.cvtColor(resized_im, cv2.COLOR_BGR2GRAY)
# blurred = cv2.GaussianBlur(gray, (11, 11), 0)

thresh = cv2.threshold(gray, 200, 255, cv2.THRESH_BINARY)[1]

text = image_to_string(thresh)

print(text)

if "emai" in text or "required" in text or "why" in text or "also" in text:
    print("detected email")

cv2.imshow("thresh", thresh)
while (cv2.waitKey() != ord('q')):
    continue
cv2.destroyAllWindows()