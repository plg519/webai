import pyautogui
from pyautogui import tweens
import time
import cv2
import numpy as np
from Classes.RegionDetector import RegionDetector
from Classes.ColorImage import ColorImage

time.sleep(3)

screenshot = pyautogui.screenshot()

# print("screenshot: ", screenshot)
# screenshot.show()

cv_im = np.array(screenshot)
cv_im = cv2.cvtColor(cv_im, cv2.COLOR_BGR2RGB)
test_im = ColorImage(pixel_values=cv_im)
# print("test_im", test_im.pixel_values.size)

resized_im = test_im.resize(height=800)
# print("resized_im", resized_im.pixel_values)

# resized_im.show()

detector = RegionDetector()


# dilation_x=2
# dilation_y=5
# dilation_num_iterations=6

# detected_regions = detector.detect_regions(resized_im, dilation_x, dilation_y, dilation_num_iterations)

dilation_x = 2
dilation_y = 3
dilation_num_iterations = 5
side_margin_ratio = 0.2

detected_regions = detector.detect_linkedin_connect(resized_im, dilation_x, dilation_y, dilation_num_iterations, side_margin_ratio)


# for region in detected_regions:
#     region.show()

resized_im.add_detected_regions(detected_regions)
resized_im.draw_detected_regions("connect_button")

resized_im.show()
