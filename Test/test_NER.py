import spacy
from spacy.matcher import Matcher, PhraseMatcher
from Classes.FirestoreHandler import FirestoreHandler
import re

nlp = spacy.load('en_core_web_sm')


def recognize_name_entities(matches):
    for match_id, start, end in matches:
        string_id = nlp.vocab.strings[match_id]  # get string representation
        span = doc[start:end]  # the matched span
        print(span.text, " ", string_id)







# doc = nlp(u'Hello, world! Hello world!')
# doc = nlp(u'I am looking for some tutors who can teach english writing')
# doc = nlp(u'I am looking for some tutors who can teach Japanese education and web design, essay')
# doc = nlp(u"I'm looking for a math tutor with reasonable rate")
# doc = nlp(u"I'm looking for a experienced SAT tutor for my son.")
# doc = nlp(u"I'm looking for a highly reviewed english writing tutor, the rate should be less than $100 per hour.")
# doc = nlp(u"I'm looking for a certified elementary math tutor for my daughter, must be a female, rate is not a problem.")
# doc = nlp(u"I'm looking for a biology tutuor, it would be great if he/she can also teach chemistry.")

doc = nlp(u'I need a professional piano tutor with at least 5 years experience in Plano area.')
# doc = nlp(u"I'm looking for a biology tutuor, it would be great if he/she can also teach chemistry and gradauted from columbia")
# doc = nlp(u"I'm looking for a certified elementary math tutor for my daughter, must be a female, rate is not a problem.")



basepath = r"/Users/plg519/WebAI/data_and_models"
cred_path = basepath + "/tutorsdb-f22d1-7d01c499c3af.json"

firestore_handler = FirestoreHandler(cred_path)

subjects = firestore_handler.get_all_values_by_attribute(u'tutors', 'subjects')
print('all_subjects has ', len(subjects), 'subjects \n', subjects)# print("subjects: ", subjects)
lower_case_subjects = [s.lower() for s in subjects]
# print("lower_case_subjects: ", lower_case_subjects)


matcher = Matcher(nlp.vocab)

# add match ID "HelloWorld" with no callback and one pattern
# pattern = [{'LOWER': 'hello'}, {'IS_PUNCT': True}, {'LOWER': 'world'}]
# pattern = [{'LOWER': 'english'}, {'LOWER': 'writing'}]

for subject in lower_case_subjects:
    # matcher.add('english_writing', None, pattern)
    subjects_pattern = [{'LOWER': subject}]
    matcher.add("subjects", None, subjects_pattern)

# experiences = ['experience'] #need fuzzy match nurimic followed by "years experience, etc."
# for experience in experiences:
#     # matcher.add('english_writing', None, pattern)
#     experiences_pattern = [{'LOWER': experience}]
#     matcher.add("experience", None, experiences_pattern)




locations = ['plano', 'kansas city', 'bay area'] #need public db of locations, or VisionCrawler to retrive all locations
for location in locations:
    locations_pattern = [{'LOWER': location}]
    matcher.add('location', None, locations_pattern)




#hourly_rate: detect words such as '$', 'dollar', 'rate'

#educations, same as subjects
educations = firestore_handler.get_all_values_by_attribute(u'tutors', 'educations')
print('all_educations has ', len(educations), 'educations \n', educations)# print("subjects: ", subjects)
lower_case_educations = [s.lower() for s in educations]
print("lower_case_educations: ", lower_case_educations)

for education in lower_case_educations:
    # matcher.add('english_writing', None, pattern)
    educations_pattern = [{'LOWER': education}]
    matcher.add("educations", None, educations_pattern)

#gender: detect "male/female/girl/boy/etc"
genders = ['male', 'female']
for gender in genders:
    gender_pattern = [{'LOWER': gender}]
    matcher.add('genders', None, gender_pattern)

# print("matcher: ", matcher)

matches = matcher(doc)
recognize_name_entities(matches)

phrase_matcher = PhraseMatcher(nlp.vocab)

experience_list = ['years experience']
exp_pattern= [nlp.make_doc(text) for text in experience_list]
phrase_matcher.add('experience', None, *exp_pattern)
matches = phrase_matcher(doc)
for match_id, start, end in matches:
    span = doc[start:end]
    print(span.text)

# for exp_match in re.finditer(experience_pattern, doc.text):
#     start, end = exp_match.span()
#     print("start: ", start, " end: ", end)
#     span = doc.char_span(start, end)
#     print(span.text)


