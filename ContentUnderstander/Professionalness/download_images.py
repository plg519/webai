import requests
import pandas as pd
from os.path import join
import glob
import random
from shutil import copyfile


def download_linkedin_ims(profile_name_url_path, profile_photos_save_dir):

    profile_name_url = pd.read_csv(profile_name_url_path, sep="\t", encoding="ISO-8859-1")

    print(profile_name_url)

    for index, row in profile_name_url.iterrows():
        name = str(row[1])
        url = row[2]

        try:
            if url != "ghost-person":
                response = requests.get(url)
                save_file_path= join(profile_photos_save_dir, name + ".png")
                if response.status_code == 200:
                    with open(save_file_path, "wb") as f:
                        f.write(response.content)

        except:
            print("failed to download ", url)
            continue


def copy_sampled_files_from_folder_recursively(source_dir, destination_dir, sample_size):
    all_paths = glob.glob(join(source_dir, '**/*.jpg'), recursive=True)
    selected_paths = random.sample(all_paths, sample_size)
    # print(selected_paths)
    for path in selected_paths:
        # des_path = join(destination_dir, path.split("/")[-1])
        file_name_with_slash = path.split("/")[-5:]
        des_filename = ""
        for part in file_name_with_slash:
            des_filename += part
        # print(des_filename)
        des_path = join(destination_dir, des_filename)

        print("copy ", path, " to ", des_path)

        copyfile(path, des_path)


# profile_name_url_path = r"/Users/plg519/WebAI_data/Ace/Professionalness/image_url.txt"
# profile_photos_save_dir = r"/Users/plg519/WebAI_data/Ace/Professionalness/linkedin_photos"
# download_linkedin_ims(profile_name_url_path, profile_photos_save_dir)

wild_face_dir = r"/Users/plg519/WebAI_data/Ace/Professionalness/face_in_the_wild/originalPics/2002"
sampled_save_dir = r"/Users/plg519/WebAI_data/Ace/Professionalness/sampled_wild_faces"
sample_size = 850
copy_sampled_files_from_folder_recursively(wild_face_dir, sampled_save_dir, sample_size)