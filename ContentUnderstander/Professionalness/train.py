import cv2
from collections import defaultdict

# coding: utf-8

# import plaidml.keras
# plaidml.keras.install_backend()

import scipy.ndimage
import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cross_validation import train_test_split

from keras.models import Sequential, Model
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense,BatchNormalization
from keras.callbacks import ModelCheckpoint
from keras.callbacks import EarlyStopping
from keras import applications
from keras.optimizers import Adam
from keras.utils import to_categorical


img_width, img_height = 350,350

#for positive data
pos_ims_dir = r"/Users/plg519/WebAI_data/Ace/Professionalness/linkedin_photos/*.png"
neg_ims_dir = r"/Users/plg519/WebAI_data/Ace/Professionalness/sampled_wild_faces/*.jpg"
model_save_path = r"/Users/plg519/WebAI_data/Ace/Professionalness/model/weights.best.from_scratch_all_layers.hdf5"

image_list = []
filenames = []
filename_label_d = defaultdict(int)
# for filename in glob.glob('data/Images/*.jpg'): #assuming jpg images
for filename in glob.glob(pos_ims_dir): #assuming png images
    im=scipy.ndimage.imread(filename)
    if im.shape[-1] == 3:
        im = cv2.resize(im, (img_width, img_height))
        # print("pos_im.shape: ", im.shape)
        image_list.append(im)
        filename_label_d[filename] = 1
        filenames.append(filename)

for filename in glob.glob(neg_ims_dir): #assuming png images
    im=scipy.ndimage.imread(filename)
    if im.shape[-1] == 3:
        im = cv2.resize(im, (img_width, img_height))
        # print("neg_im.shape: ", im.shape)
        image_list.append(im)
        filename_label_d[filename] = 0
        filenames.append(filename)


myarray = np.asarray(image_list)
# np.save('myarray.npy',myarray)

imgplot = plt.imshow(myarray[1])
plt.show()


Y = []
for filename in filenames:
    Y.append(filename_label_d[filename])

Y = to_categorical(Y)

# np.set_printoptions(threshold=np.nan)
print("myaray: ", myarray.shape)
print("Y: ", Y)

X_train, X_test, y_train, y_test = train_test_split(myarray, Y, test_size=0.25, random_state=42)

print("X_train: ", X_train.shape)
print("y_train: ", len(y_train))
print("X_test: ", X_test.shape)
print("y_test: ", len(y_test))

resnet=applications.resnet50.ResNet50(include_top=False, weights='imagenet',input_shape=(img_width,img_height,3),pooling='max')

model=Sequential()
model.add(resnet)
model.add(Dense(2))

print(model.summary())

# compile the model
# epochs = 30
epochs = 10

learning_rate = 0.001
opt = Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=None)
# model.compile(loss='mean_squared_error', optimizer=opt,metrics=['mae'])
model.compile(loss='categorical_crossentropy', optimizer=opt,metrics=['accuracy'])

checkpointer = ModelCheckpoint(filepath=model_save_path,
                               verbose=1, save_best_only=True)

stopper = EarlyStopping(monitor='val_loss',
                              min_delta=0,
                              patience=10,
                              verbose=0, mode='auto')

model.fit(X_train,y_train,epochs=epochs,batch_size=32,validation_split=0.2, callbacks=[checkpointer,stopper], verbose=1, shuffle=False)


