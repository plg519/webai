import cv2
import copy
from Classes.LineDetector import LineDetector
import numpy as np
from pytesseract import image_to_string


class RectangleRegion:
    def __init__(self, x, y, w, h, object_category = None, pixel_values = None, sub_regions = [], text = "", xy_cube = 0, region_type=""):
        self._x = x
        self._y = y
        self._w = w
        self._h = h
        self._object_category = object_category # String
        self._pixel_values = pixel_values # 3D Numpy Array
        self._sub_regions = list(sub_regions) # list of RectangleRegion
        self._text = text # String
        self._xy_cube = xy_cube # float
        self._region_type = region_type # String

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, new_x):
        self._x = new_x

    @property
    def y(self):
        return self._y
    @y.setter
    def y(self, new_y):
        self._y = new_y

    @property
    def w(self):
        return self._w
    @w.setter
    def w(self, new_w):
        self._w = new_w

    @property
    def h(self):
        return self._h
    @h.setter
    def h(self, new_h):
        self._h = new_h

    @property
    def object_category(self):
        return self._object_category
    @object_category.setter
    def object_category(self, new_object_category):
        self._object_category = new_object_category

    @property
    def pixel_values(self):
        return self._pixel_values
    @pixel_values.setter
    def pixel_values(self, new_pixel_values):
        self._pixel_values = new_pixel_values

    @property
    def sub_regions(self):
        return self._sub_regions
    @sub_regions.setter
    def sub_regions(self, new_sub_regions):
        self._sub_regions = new_sub_regions

    @property
    def text(self):
        return self.text
    @text.setter
    def text(self, new_text):
        self._text = new_text

    @property
    def xy_cube(self):
        return self._xy_cube
    @xy_cube.setter
    def xy_cube(self, new_xy_tube):
        self.xy_cube = new_xy_tube

    @property
    def region_type(self):
        return self._region_type

    @region_type.setter
    def region_type(self, new_region_type):
        self._region_type = new_region_type


    def set_xy_tube(self):
        self._xy_cube = self._x * (self._y ** 3)

    def add_sub_region(self, new_sub_region):
        self._sub_regions.append(new_sub_region)

    def add_sub_regions(self, new_sub_regions):
        self._sub_regions.extend(new_sub_regions)

    def show(self):
        cv2.imshow("", self._pixel_values)
        while (cv2.waitKey() != ord('q')):
            continue
        cv2.destroyAllWindows()

    def is_close(self, other, vertical_distance_threshold, horizontal_distance_threshold):
        if abs(self._x - other._x) < horizontal_distance_threshold \
           and abs(self._y - other._y) < vertical_distance_threshold\
           and self._y - other._y < 0 :
            return True
        else:
            return False

    def is_horizontal_aligned(self, other, vertical_distance_threshold, horizontal_distance_threshold):
        if abs(self._x - other._x) < horizontal_distance_threshold \
           and abs(self._y - other._y) < vertical_distance_threshold:
            return True
        else:
            return False

    def find_middle_region(self, color_image, top_region, bottom_region):
        mid_x = min(top_region._x, bottom_region._x)
        mid_y = top_region._y + top_region._h
        mid_w = max(top_region._w, bottom_region._w)

        # print("bottom_region._y: ", bottom_region._y, "top_region._y: ", top_region._y, "top_region._h: ", top_region._h)

        mid_h = bottom_region._y - (top_region._y + top_region._h)
        # mid_h = 10
        # print("mid_h: ", mid_h)
        middle_region = RectangleRegion(mid_x,
                                        mid_y,
                                        mid_w,
                                        mid_h,
                                        "middle",
                                        color_image._pixel_values[mid_y:mid_y+mid_h, mid_x:mid_x+mid_w])
        # middle_region.show()
        return middle_region

    def find_horizontal_middle_region(self, color_image, left_region, right_region):
        # mid_x = left_region._x + left_region._w + 8
        mid_x = left_region._x + left_region._w

        # mid_y = left_region._y
        mid_y = left_region._y - 8

        # mid_w = right_region._x - (left_region._x + left_region._w + 5)
        mid_w = right_region._x - (left_region._x + left_region._w + 5)


        # print("bottom_region._y: ", bottom_region._y, "top_region._y: ", top_region._y, "top_region._h: ", top_region._h)

        # mid_h = int(left_region._h * 2.5)
        mid_h = int(left_region._h * 2.5) + 8

        # mid_h = 10
        # print("mid_h: ", mid_h)
        middle_region = RectangleRegion(mid_x,
                                        mid_y,
                                        mid_w,
                                        mid_h,
                                        "middle",
                                        color_image._pixel_values[mid_y:mid_y+mid_h, mid_x:mid_x+mid_w])
        # middle_region.show()
        return middle_region

    def add_middle_subregion(self, color_image, top_region_cate, bottom_region_cate):
        for sub_region in self._sub_regions:
            if sub_region._object_category == top_region_cate:
                top_subregion = copy.deepcopy(sub_region)
            if sub_region._object_category == bottom_region_cate:
                bottom_subregion = copy.deepcopy(sub_region)

        # top_subregion.show()
        # bottom_subregion.show()

        middle_subregion = self.find_middle_region(color_image, top_subregion, bottom_subregion)
        self._sub_regions.append(middle_subregion)


    def add_horizontal_middle_subregion(self, color_image, left_region_cate, right_region_cate):
        for sub_region in self._sub_regions:
            if sub_region._object_category == left_region_cate:
                left_subregion = copy.deepcopy(sub_region)
            if sub_region._object_category == right_region_cate:
                right_subregion = copy.deepcopy(sub_region)

        # top_subregion.show()
        # bottom_subregion.show()

        middle_subregion = self.find_horizontal_middle_region(color_image, left_subregion, right_subregion)
        self._sub_regions.append(middle_subregion)

    def find_cate_region(self, subregion_cate):
        for subregioin in self._sub_regions:
            # print("subregion")
            # subregioin.show()
            if subregioin._object_category == subregion_cate:
                # print("match")
                # subregioin.show()
                return subregioin

    def add_position_connections_subsubregions(self, color_image, connect_button_region):
        lines = LineDetector.detect_horizontal_lines(self._pixel_values)

        wanted_line = []
        # for line in lines[0]:
        #     pt1 = (line[0], line[1])
        #     pt2 = (line[2], line[3])
        #     line_y = line[1]
        #     line_long = line[2] - line[0]
        #     # print("line_y: ", line_y)
        #     # print("line_long: ", line_long)
        #     # print("self._h: ", self._h)
        #     # print("self._w: ", self._w)
        #     if line_y > self._h/3.0 and line_y < self._h * (2/3.0) and line[0] < 10 and line_long > self._w/3.0:
        #         wanted_line = line
        #         break

        # print("wanted_line: ", wanted_line)
        # if wanted_line == []: #if did not found qualified line, use middle line as default
        #     wanted_line = [0, int(self._h/2), int(self._w/2), int(self._h/2)]
        # wanted_line = [0, int(self._h / 2), int(self._w / 2), int(self._h / 2)]
        wanted_line = [0, int(self._h * 0.95), int(self._w / 2), int(self._h * 0.95)]


        # line = lines[0][0]
        # print("wanted_line: ", wanted_line)
        pt1 = (wanted_line[0], wanted_line[1])
        pt2 = (wanted_line[2], wanted_line[3])
        cv2.line(self._pixel_values, pt1, pt2, (0, 0, 255), 3)

        line_y = wanted_line[1] #y-axis of the line

        name_position_region_x = self._x
        name_position_region_y = self._y
        name_position_region_w = self._w
        name_position_region_h = line_y

        name_position_region = RectangleRegion(name_position_region_x,
                                               name_position_region_y,
                                               name_position_region_w,
                                               name_position_region_h, #height in big color_image is the y in local small middle region
                                                "name_position",
                                                color_image._pixel_values[name_position_region_y:name_position_region_y+name_position_region_h,
                                                name_position_region_x:name_position_region_x+name_position_region_w])

        connections_region_x = self._x
        connections_region_y = self._y + line_y
        connections_region_w = self._w
        connections_region_h = connect_button_region._y - connections_region_y

        connections_region = RectangleRegion(connections_region_x,
                                             connections_region_y,
                                             connections_region_w,
                                             connections_region_h, #height in big color_image is the y in local small middle region
                                                "connections",
                                                color_image._pixel_values[connections_region_y:connections_region_y+connections_region_h,
                                                connections_region_x:connections_region_x+connections_region_w])

        self._sub_regions.append(name_position_region)
        self._sub_regions.append(connections_region)

    def add_name_connections_subsubregions(self, color_image):
        #to-do: 1: use name detector to find name region, then use name region bottem to define the line, to separate to name_degree and description region to add
        dilation_x = 2
        dilation_y = 3
        dilation_num_iterations = 3
        # RD.RegionDetector.detect_search_linkedin_name_degree(color_image, dilation_x, dilation_y, dilation_num_iterations)

        detected_regions = []

        # im = color_image.pixel_values
        im = self.pixel_values

        img_final = im.copy()
        ori_y, ori_x, ori_num_chanels = im.shape

        # print("ori_y: ", ori_y, " ori_x ", ori_x)

        # print(im[567:690, 410:563])
        # cv2.imshow("connect: ",im[567:690, 410:563])
        # while (cv2.waitKey() != ord('q')):
        #     continue
        # cv2.destroyAllWindows()

        upper_blue = np.array([180, 121, 21])
        # lower_blue = np.array([170, 111, 11])
        lower_blue = np.array([170, 111, 0])

        # upper_blue = np.array([167, 100, 17])
        # lower_blue = np.array([157, 90, 7])

        # lower_blue = np.array([0, 40, 50])
        # upper_blue = np.array([110, 180, 250])

        hsv_im = cv2.cvtColor(im, cv2.COLOR_BGR2HSV)
        # cv2.imshow("hsv_im", hsv_im)

        #
        # # x1, y1, x2, y2 = 456, 683, 465, 694
        # x1, y1, x2, y2 = 458, 684, 460, 686
        #
        #
        # connect_im = hsv_im[y1:y2, x1:x2]
        #
        # print("connect_im: ", cv2.cvtColor(im[y1:y2, x1:x2], cv2.COLOR_BGR2HSV))
        #
        # cv2.imshow("connect_im", im[y1:y2, x1:x2])
        #
        #
        # while (cv2.waitKey() != ord('q')):
        #     continue
        # cv2.destroyAllWindows()

        # bw_im = cv2.threshold(res, 10, 255, cv2.THRESH_BINARY)[1]

        # lower_blue = np.array([50, 50, 110])
        # upper_blue = np.array([255, 255, 130])

        lower_blue = np.array([90, 50, 50])
        upper_blue = np.array([130, 255, 255])

        mask = cv2.inRange(hsv_im, lower_blue, upper_blue)
        res = cv2.bitwise_and(im, im, mask=mask)

        # mask = cv2.inRange(im, lower_blue, upper_blue)
        # blue_im = cv2.bitwise_and(im, im, mask=mask)
        gray_im = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)

        # cv2.imshow("blue_im", res)
        # while (cv2.waitKey() != ord('q')):
        #     continue
        # cv2.destroyAllWindows()

        bw_im = cv2.threshold(gray_im, 50, 255, cv2.THRESH_BINARY)[1]
        # cv2.imshow("bw_im", bw_im)
        # input("press to continue")


        kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (dilation_x, dilation_y))
        dilated_im = cv2.dilate(bw_im, kernel, iterations=dilation_num_iterations)
        _, contours, hierarchy = cv2.findContours(dilated_im, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        # cv2.imshow("dilated_im", dilated_im)
        # while (cv2.waitKey() != ord('q')):
        #     continue
        # cv2.destroyAllWindows()

        # input("press to continue")

        name_contours = []

        for contour in contours:
            # get rectangle bounding contour
            [x, y, w, h] = cv2.boundingRect(contour)

            # Don't plot small false positives that aren't text
            if w / float(h) < 1:
                continue

            # cropped_im = img_final[int(y + h / 5.0):int(y + h - h / 5.0), int(x + w * 0.25):int(x + w - w * 0.25)]
            # cropped_im = img_final[y:y+h, x:x+w]
            cropped_im = img_final[y:y+h, (x-5):x+w]


            # cv2.imshow(image_to_string(cv2.resize(cropped_im, (cropped_im.shape[1]*2, cropped_im.shape[0]*2))), cropped_im)
            # while (cv2.waitKey() != ord('q')):
            #     continue
            # cv2.destroyAllWindows()

            # print(image_to_string(cv2.resize(cropped_im, (cropped_im.shape[1]*2, cropped_im.shape[0]*2))))
            # input("press to continue")

            if image_to_string(cv2.resize(cropped_im, (cropped_im.shape[1]*2, cropped_im.shape[0]*2))) == "":
                continue

            # if "Connec" not in image_to_string(cv2.resize(cropped_im, (cropped_im.shape[1]*2, cropped_im.shape[0]*2))):
            # if "nne" not in image_to_string(cv2.resize(cropped_im, (cropped_im.shape[1]*2, cropped_im.shape[0]*2))):
            #     continue

            name_contours.append(contour)

        # print("len(name_contours): ", len(name_contours))

        if len(name_contours) > 2 or len(name_contours) < 1:
            return

        if len(name_contours) == 2:
            if cv2.boundingRect(name_contours[0])[0] > cv2.boundingRect(name_contours[1])[0]:
                # print("cv2.boundingRect(name_contours[0])[0]: ", cv2.boundingRect(name_contours[0])[0])
                name_contours[0], name_contours[1] = name_contours[1], name_contours[0]

        [x, y, w, h] = cv2.boundingRect(name_contours[0])
        # name_degree_region_x = self._x + x
        name_degree_region_x = self._x + x - 5

        name_degree_region_y = self._y + y
        # name_degree_region_w = w
        name_degree_region_w = w + 5

        name_degree_region_h = h

        name_degree_region = RectangleRegion(name_degree_region_x, name_degree_region_y, name_degree_region_w, name_degree_region_h,
                                             "name",
                                             color_image._pixel_values[name_degree_region_y:name_degree_region_y+name_degree_region_h,
                                             name_degree_region_x:name_degree_region_x+name_degree_region_w])



        title_location_skill_connection_region_x = name_degree_region_x
        title_location_skill_connection_region_y = name_degree_region_y + name_degree_region_h
        title_location_skill_connection_region_w = self._w
        title_location_skill_connection_region_h = self._h - name_degree_region_h

        title_location_skill_connection_region = RectangleRegion(title_location_skill_connection_region_x,
                                                                 title_location_skill_connection_region_y,
                                                                 title_location_skill_connection_region_w,
                                                                 title_location_skill_connection_region_h,
                                                                    "title_location_skill_connection",
                                                                color_image._pixel_values[
                                                                title_location_skill_connection_region_y:title_location_skill_connection_region_y + title_location_skill_connection_region_h,
                                                                title_location_skill_connection_region_x:title_location_skill_connection_region_x + title_location_skill_connection_region_w])

    # while (cv2.waitKey() != ord('q')):
    #     continue
    # cv2.destroyAllWindows()

        self._sub_regions.append(name_degree_region)
        self._sub_regions.append(title_location_skill_connection_region)

    def ocr(self):
        # self._text = image_to_string(cv2.resize(self._pixel_values, (self._pixel_values.shape[1]*2, self._pixel_values.shape[0]*2)), lang="eng")
        self._text = image_to_string(cv2.resize(self._pixel_values, (self._pixel_values.shape[1]*2, self._pixel_values.shape[0]*2)), lang="eng")
