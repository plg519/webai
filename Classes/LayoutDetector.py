from Classes.RegionDetector import RegionDetector

class LayoutDetector:

    @staticmethod
    def add_detected_regions(im):
        detector = RegionDetector()
        dilation_x = 3
        dilation_y = 2
        dilation_num_iterations = 5

        detected_regions = detector.detect_ace_profile_regions(im, dilation_x, dilation_y, dilation_num_iterations)
        im.add_detected_regions(detected_regions)
        im.ocr_general_regions()


    @staticmethod
    def find_common_field_names(ims):

        detector = RegionDetector()
        dilation_x = 3
        dilation_y = 2
        dilation_num_iterations = 5

        for im in ims:
            detected_regions = detector.detect_ace_profile_regions(im, dilation_x, dilation_y, dilation_num_iterations)
            im.add_detected_regions(detected_regions)
            im.ocr_general_regions()
            # im.show()

        assert(len(ims) >= 2)

        #convert each region text from a image to a set
        im_text_sets = []
        for im in ims:
            im_text_set = set()
            for region in im._detected_regions:
                im_text_set.add(region._text)
            im_text_sets.append(im_text_set)

        # common_filed_names = im_text_sets[0].intersect(im_text_sets(1))
        # if(len(im_text_sets) > 2):
        #     for i in range(2, len(im_text_sets)):
        #         common_filed_names = common_filed_names.intersect(im_text_sets[i])

        # for im_text in im_text_sets:
        #     print("im_text: ", im_text)

        common_filed_names = set.intersection(*im_text_sets)

        return common_filed_names

    @staticmethod
    def add_common_field_regions(ims, common_field_names):
        for im in ims:
            im.add_common_field_regions(common_field_names)

