import cv2
from Classes.RectangleRegion import RectangleRegion
# from Classes.VisionCrawler import VisionCrawler
import time
import copy
# import pyautogui
import numpy as np
from pytesseract import image_to_string
from collections import defaultdict
import re
from AceTutor.Tutor import Tutor


class ColorImage:
    def __init__(self, pixel_values=None, detected_regions=[]):
        self._pixel_values = pixel_values  # 3D Numpy Array
        self._detected_regions = list(detected_regions)  # list of RectangleRegion
        # self._scrolled_to_top = scrolled_to_top

    @property
    def pixel_values(self):
        return self._pixel_values
    @pixel_values.setter
    def pixel_values(self, new_pixel_values):
        self._pixel_values = new_pixel_values

    # @property
    # def scrolled_to_top(self):
    #     return self._scrolled_to_top
    # @scrolled_to_top.setter
    # def scrolled_to_top(self, new_scrolled_to_top):
    #     self._scrolled_to_top = new_scrolled_to_top

    def show(self):
        cv2.imshow("", self.pixel_values)
        while (cv2.waitKey() != ord('q')):
            continue
        cv2.destroyAllWindows()

    def draw_all_detected_regions(self):
        for region in self._detected_regions:
            [x, y, w, h] = region.x, region.y, region.w, region.h
            # copy = self._pixel_values.transpose((1, 2, 0)).astype(np.uint8).copy()
            cv2.rectangle(self._pixel_values, (x, y), (x + w, y + h), (255, 0, 255), 2)
            # self._pixel_values = copy

    def draw_detected_regions(self, category):
        for region in self._detected_regions:
            if region._object_category == category:
                [x, y, w, h] = region.x, region.y, region.w, region.h
                cv2.rectangle(self._pixel_values, (x, y), (x + w, y + h), (255, 0, 255), 2)

    def draw_detected_regions_with_type(self, type):
        for region in self._detected_regions:
            if region._region_type == type:
                [x, y, w, h] = region.x, region.y, region.w, region.h
                cv2.rectangle(self._pixel_values, (x, y), (x + w, y + h), (255, 0, 255), 2)

    def draw_detected_regions_with_subregions(self, category):
        for region in self._detected_regions:
            if region._object_category == category:
                [x, y, w, h] = region.x, region.y, region.w, region.h
                cv2.rectangle(self._pixel_values, (x, y), (x + w, y + h), (255, 0, 255), 3)
                for sub_region in region._sub_regions:
                    # if sub_region._object_category == "middle":
                    [x, y, w, h] = sub_region.x, sub_region.y, sub_region.w, sub_region.h
                    cv2.rectangle(self._pixel_values, (x, y), (x + w, y + h), (255, 255, 0), 2)
                    for sub_sub_region in sub_region._sub_regions:
                        [x, y, w, h] = sub_sub_region.x, sub_sub_region.y, sub_sub_region.w, sub_sub_region.h
                        cv2.rectangle(self._pixel_values, (x, y), (x + w, y + h), (0, 255, 255), 1)

    def resize(self, width = None, height = None, inter = cv2.INTER_AREA):
        # initialize the dimensions of the image to be resized and
        # grab the image size
        dim = None
        (h, w) = self.pixel_values.shape[:2]

        # if both the width and height are None, then return the
        # original image
        if width is None and height is None:
            return ColorImage(pixel_values = self.pixel_values)

        # check to see if the width is None
        if width is None:
            # calculate the ratio of the height and construct the
            # dimensions
            r = height / float(h)
            dim = (int(w * r), height)

        # otherwise, the height is None
        else:
            # calculate the ratio of the width and construct the
            # dimensions
            r = width / float(w)
            dim = (width, int(h * r))

        # resize the image
        resized = cv2.resize(self.pixel_values, dim, interpolation = inter)

        # return the resized image
        return ColorImage(pixel_values=resized)

    def add_detected_regions(self, new_regions):
        self._detected_regions.extend(new_regions)

    def merge_photo_connect_to_profile(self, v_thresh, h_thresh):
        photo_regions = []
        connect_regions = []
        for region in self._detected_regions:
            # print("region._y ", region._y)
            if region._object_category == "profile_photo":
                photo_regions.append(region)
            if region._object_category == "connect_button":
                connect_regions.append(region)

        for photo_region in photo_regions:
            for connect_region in connect_regions:

                if photo_region.is_close(connect_region, v_thresh, h_thresh):
                    profile_x = min(photo_region._x, connect_region._x)
                    # profile_x = min(photo_region._x, connect_region._x)  - 25

                    # print("photo_region._y ", photo_region._y)
                    profile_y = photo_region._y

                    # print("profile_y: ", profile_y)

                    # profile_w = connect_region._w
                    # profile_w = max(photo_region._w, connect_region._w) + 55
                    profile_w = max(photo_region._w, connect_region._w)


                    profile_h = connect_region._y - photo_region._y + connect_region._h
                    # print(profile_x, " ", profile_y, " ", profile_w, " ", profile_h)
                    profile_region = RectangleRegion(profile_x, profile_y, profile_w, profile_h, object_category="profile",
                                                     pixel_values = self._pixel_values[profile_y:profile_y + profile_h, profile_x:profile_x+ profile_w])
                    profile_region.add_sub_region(photo_region)
                    profile_region.add_sub_region(connect_region)


                    self._detected_regions.append(profile_region)

    def merge_search_photo_connect_to_profile(self, v_thresh, h_thresh):
        photo_regions = []
        connect_regions = []
        for region in self._detected_regions:
            # print("region._y ", region._y)
            if region._object_category == "profile_photo":
                photo_regions.append(region)
            if region._object_category == "connect_button":
                connect_regions.append(region)

        for photo_region in photo_regions:
            for connect_region in connect_regions:
                # print("photo_region: ", photo_region._x, " ", photo_region._y, " ", photo_region._w, " ", photo_region._h)
                # print("connect_region: ", connect_region._x, " ", connect_region._y, " ", connect_region._w, " ", connect_region._h)

                if photo_region.is_horizontal_aligned(connect_region, v_thresh, h_thresh):
                    # print("yes")
                    profile_x = min(photo_region._x, connect_region._x)
                    # profile_x = min(photo_region._x, connect_region._x)  - 25

                    # print("photo_region._y ", photo_region._y)
                    profile_y = photo_region._y

                    # print("profile_y: ", profile_y)

                    # profile_w = connect_region._w
                    # profile_w = max(photo_region._w, connect_region._w) + 55
                    profile_w = connect_region._x - photo_region._x + connect_region._w


                    # profile_h = connect_region._y - photo_region._y + connect_region._h
                    profile_h = int(photo_region._h * 2.5)

                    # print(profile_x, " ", profile_y, " ", profile_w, " ", profile_h)
                    profile_region = RectangleRegion(profile_x, profile_y, profile_w, profile_h, object_category="profile",
                                                     pixel_values = self._pixel_values[profile_y:profile_y + profile_h, profile_x:profile_x+ profile_w])
                    profile_region.add_sub_region(photo_region)
                    profile_region.add_sub_region(connect_region)


                    self._detected_regions.append(profile_region)

    def add_photo_connect_middle_subregion(self, top_region_cate, bottom_region_cate):
        for region in self._detected_regions:
            if region._object_category == "profile":
                region.add_middle_subregion(self, top_region_cate, bottom_region_cate)

    def add_search_photo_connect_middle_subregion(self, left_region_cate, right_region_cate):
        for region in self._detected_regions:
            if region._object_category == "profile":
                region.add_horizontal_middle_subregion(self, left_region_cate, right_region_cate)



    def add_positions_connections_subregions(self):
        for region in self._detected_regions:
            if region._object_category == "profile":
                # print("profile")
                # region.show()
                connect_region = region.find_cate_region("connect_button")
                middle_region = region.find_cate_region("middle")
                # print("middle")
                # middle_region.show()
                middle_region.add_position_connections_subsubregions(self, copy.deepcopy(connect_region))

    def add_name_connections_subregions(self):
        for region in self._detected_regions:
            if region._object_category == "profile":
                # print("profile")
                # region.show()
                connect_region = region.find_cate_region("connect_button")
                middle_region = region.find_cate_region("middle")
                # print("middle")
                # middle_region.show()
                middle_region.add_name_connections_subsubregions(self)



    def ocr_position_regions(self):
        # return
        for region in self._detected_regions:
            if region._object_category == "profile":
                # print("ocr start")
                # region.show()
                middle_region = region.find_cate_region("middle")
                # middle_region.show()
                position_region = middle_region.find_cate_region("name_position")
                position_region.ocr()

                # print("position_region._text: ", position_region._text)
                # position_region.show()

    def ocr_name_regions(self):
        # return
        for region in self._detected_regions:
            if region._object_category == "profile":
                # print("ocr start")
                # region.show()
                middle_region = region.find_cate_region("middle")
                # middle_region.show()
                name_region = middle_region.find_cate_region("name")
                if name_region:
                    name_region.ocr()

                # print("position_region._text: ", name_region._text)
                # name_region.show()

    def ocr_general_regions(self):
        for region in self._detected_regions:
            region.ocr()

    def get_profile_regions_by_reverse_order(self):
        profile_regions = []
        for region in self._detected_regions:
            if region._object_category == "profile":
                region.set_xy_tube()
                profile_regions.append(copy.deepcopy(region))

        profile_regions.sort(key=lambda x: x._xy_cube, reverse=True)
        return profile_regions

    def sort_regions_by_top_down_order(self):
        # for region in self._detected_regions:
        #     region.set_xy_tube()
        self._detected_regions.sort(key=lambda x: x._y, reverse=False)

    def get_profile_regions_by_top_down_order(self):
        profile_regions = []
        for region in self._detected_regions:
            if region._object_category == "profile":
                region.set_xy_tube()
                profile_regions.append(copy.deepcopy(region))

        profile_regions.sort(key=lambda x: x._xy_cube, reverse=False)
        return profile_regions



    def add_common_field_regions(self, common_field_names):
        for region in self._detected_regions:
            if  region._text.strip() != "" and region._text in common_field_names:
            # if  region._text.strip() != "" and "_" in region._text.strip()  and region._text in common_field_names:
                # print("region._text: ", region._text, " common_field_names ", common_field_names)
                region._region_type = "common_fields"


    def build_ace_field_name_content_dict(self):
        d = defaultdict(list)
        # d['profile_photo'].append(self._detected_regions[1])
        d['profile_photo'].append(self._detected_regions[2]) #for new ace web version
        # d['profile_photo'].append(self._detected_regions[4]) #for new ace web version3


        # d['name'].append(self._detected_regions[2])
        d['name'].append(self._detected_regions[3]) #for new ace web version
        # d['name'].append(self._detected_regions[5]) #for new ace web version3


        d['location'].append(self._detected_regions[9]) #for new ace web version


        field_names_region_indexes = []
        for i in range(len(self._detected_regions)):
            if self._detected_regions[i]._region_type == "common_fields":
                field_names_region_indexes.append(i)

        for i in range(len(field_names_region_indexes) - 1):
            if field_names_region_indexes[i + 1] - field_names_region_indexes[i] > 1:
                for j in range(field_names_region_indexes[i] + 1,  field_names_region_indexes[i + 1]):
                    # d[re.sub(r'\W+', '', (self._detected_regions[field_names_region_indexes[i]]._text).strip())].append(self._detected_regions[j])
                    d[re.sub(r'[^a-zA-Z]', '', (self._detected_regions[field_names_region_indexes[i]]._text).strip())].append(self._detected_regions[j])


        return d

    def build_ace_field_name_content_dict_v3(self):
        d = defaultdict(list)
        # d['profile_photo'].append(self._detected_regions[1])
        # d['profile_photo'].append(self._detected_regions[2]) #for new ace web version
        d['profile_photo'].append(self._detected_regions[4]) #for new ace web version3


        # d['name'].append(self._detected_regions[2])
        # d['name'].append(self._detected_regions[3]) #for new ace web version
        d['name'].append(self._detected_regions[5]) #for new ace web version3


        d['location'].append(self._detected_regions[9]) #for new ace web version


        field_names_region_indexes = []
        for i in range(len(self._detected_regions)):
            if self._detected_regions[i]._region_type == "common_fields":
                field_names_region_indexes.append(i)

        for i in range(len(field_names_region_indexes) - 1):
            if field_names_region_indexes[i + 1] - field_names_region_indexes[i] > 1:
                for j in range(field_names_region_indexes[i] + 1,  field_names_region_indexes[i + 1]):
                    # d[re.sub(r'\W+', '', (self._detected_regions[field_names_region_indexes[i]]._text).strip())].append(self._detected_regions[j])
                    d[re.sub(r'[^a-zA-Z]', '', (self._detected_regions[field_names_region_indexes[i]]._text).strip())].append(self._detected_regions[j])


        return d


    def create_tutor(self):
        #finish extracting rest fileds and use them and Tutor constructor to create a Tutor object and return it

        text_regions_d = self.build_ace_field_name_content_dict()

        # print("text_regions_d: ")
        # print(text_regions_d)
        # for k, v in text_regions_d.items():
        #     print("k: ", k, " value:")
        #     for region in v:
        #         print(region._text, ", ")

        # profile_photo = text_regions_d["profile_photo"][0]._pixel_values
        profile_photo = text_regions_d["profile_photo"][0]
        name = text_regions_d["name"][0]._text
        # location = text_regions_d["Tutor"][0]._text[2:]
        # location = text_regions_d["location"][0]._text
        location = ""

        num_views_str = text_regions_d["Review"][0]._text
        # text_regions_d["Review"][0].show()
        # print("num_views_str: ", num_views_str)
        num_views = [int(s) for s in str.split(num_views_str) if s.isdigit()][0]

        description = ""
        views_idx = 0
        read_more_or_contact_idx = 0
        for i in range(len(text_regions_d["Review"])):
            if "Views" in text_regions_d["Review"][i]._text:
                views_idx = i
            # if "Read More" in text_regions_d["Review"][i]._text or "CONTACT" in text_regions_d["Review"][i]._text:
            if "Read more" in text_regions_d["Review"][i]._text or "Share" in text_regions_d["Review"][i]._text:
                read_more_or_contact_idx = i

        # print("views_idx: ", views_idx)
        # print("read_more_or_contact_idx: ", read_more_or_contact_idx)

        if views_idx >= 0 and read_more_or_contact_idx > 0 and read_more_or_contact_idx > views_idx + 1:
            for region in text_regions_d["Review"][(views_idx + 1):read_more_or_contact_idx]:
                description += region._text + " "

        # print("description: ", description)



        tutoring_experience = text_regions_d["TutoringExperience"][0]._text
        subjects = ""
        for region in text_regions_d["Subjects"]:
            # print("region._text: ", region._text)
            subjects += region._text + ". "
            # print("subjects: ", subjects)

        subjects_list = [subject.strip() for subject in subjects.split(".") if subject.strip() != ""]

        acceptable_hourly_rate_str = re.sub(r'\W+', '', text_regions_d["AcceptableHourlyRate"][0]._text)
        acceptable_hourly_rate = acceptable_hourly_rate_str.replace("HourlyRate", "")
        educations = []
        for region in text_regions_d["Educations"]:
            educations.append(region._text)
        summary = ""
        # print("text_regions_d[Summary]: ", text_regions_d["Summary"])
        for region in text_regions_d["Summary"]:
            summary += region._text + " "
            # print("summary after append: ", summary)

        highlights = ""

        tutor = Tutor(name, profile_photo, location, num_views, description, tutoring_experience, subjects_list, acceptable_hourly_rate, highlights, educations, summary)
        return tutor



    def create_tutor_v3(self, profile_url):
        #finish extracting rest fileds and use them and Tutor constructor to create a Tutor object and return it

        text_regions_d = self.build_ace_field_name_content_dict_v3()

        # print("text_regions_d: ")
        # print(text_regions_d)
        # for k, v in text_regions_d.items():
        #     print("k: ", k, " value:")
        #     for region in v:
        #         print(region._text, ", ")

        # profile_photo = text_regions_d["profile_photo"][0]._pixel_values
        profile_photo = text_regions_d["profile_photo"][0]
        name = text_regions_d["name"][0]._text
        # location = text_regions_d["Tutor"][0]._text[2:]
        # location = text_regions_d["location"][0]._text
        location = ""

        num_views_str = text_regions_d["Review"][0]._text
        # text_regions_d["Review"][0].show()
        # print("num_views_str: ", num_views_str)
        num_views = [int(s) for s in str.split(num_views_str) if s.isdigit()][0]

        description = ""
        views_idx = 0
        read_more_or_contact_idx = 0
        for i in range(len(text_regions_d["Review"])):
            if "Views" in text_regions_d["Review"][i]._text:
                views_idx = i
            # if "Read More" in text_regions_d["Review"][i]._text or "CONTACT" in text_regions_d["Review"][i]._text:
            if "Read more" in text_regions_d["Review"][i]._text or "Share" in text_regions_d["Review"][i]._text:
                read_more_or_contact_idx = i

        # print("views_idx: ", views_idx)
        # print("read_more_or_contact_idx: ", read_more_or_contact_idx)

        if views_idx >= 0 and read_more_or_contact_idx > 0 and read_more_or_contact_idx > views_idx + 1:
            for region in text_regions_d["Review"][(views_idx + 1):read_more_or_contact_idx]:
                description += region._text + " "

        # print("description: ", description)



        tutoring_experience = text_regions_d["TutoringExperience"][0]._text
        subjects = ""
        for region in text_regions_d["Subjects"]:
            # print("region._text: ", region._text)
            subjects += region._text + ". "
            # print("subjects: ", subjects)

        subjects_list = [subject.strip() for subject in subjects.split(".") if subject.strip() != ""]

        acceptable_hourly_rate_str = re.sub(r'\W+', '', text_regions_d["AcceptableHourlyRate"][0]._text)
        acceptable_hourly_rate = int(acceptable_hourly_rate_str.replace("HourlyRate", "").strip())
        educations = []
        for region in text_regions_d["Educations"]:
            educations.append(region._text)
        summary = ""
        # print("text_regions_d[Summary]: ", text_regions_d["Summary"])
        for region in text_regions_d["Summary"]:
            summary += region._text + " "
            # print("summary after append: ", summary)

        highlights = ""

        tutor = Tutor(profile_url, name, profile_photo, location, num_views, description, tutoring_experience, subjects_list, acceptable_hourly_rate, highlights, educations, summary)
        return tutor
