import numpy as np


class RectangleRegionHandler:
    def __init__(self, rectangle_region):
        self._rectangle_region = rectangle_region
        self._array_data_type = self._rectangle_region.dtype.name
        self._array_shape = self._rectangle_region.shape

    @property
    def rectangle_region(self):
        return self._rectangle_region

    @property
    def array_data_type(self):
        return self._array_data_type

    @property
    def array_shape(self):
        return self._array_shape

    def rectangle_region_to_bytes(self):
        return self._rectangle_region.tobytes()

    def bytes_to_rectangle_region(self, bytes_of_region):
        return np.frombuffer(bytes_of_region, dtype=self._array_data_type).reshape(self._array_shape)
