import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from google.cloud.firestore_v1beta1 import ArrayUnion, ArrayRemove
from google.cloud import firestore
from AceTutor.TutorAttributeFirestore import TutorAttributeFirestore


class FirestoreHandler:
    def __init__(self, credential_path):
        self._cred = credentials.Certificate(credential_path)
        firebase_admin.initialize_app(self._cred)

        self._db = firebase_admin.firestore.client()

    def save_document(self, collection_name, obj, obj_id):
        doc_ref = self._db.collection(collection_name).document(obj_id)
        doc_ref.set(obj.to_dict_firestore())

    def batch_save(self, collection_name, objs, obj_ids):
        batch = self._db.batch()

        for idx, val in enumerate(objs):
            doc_ref = self._db.collection(collection_name).document(obj_ids[idx])
            batch.set(doc_ref, val.to_dict_firestore())

        batch.commit()

    def delete_document(self, collection_name, obj_id):
        self._db.collection(collection_name).document(obj_id).delete()

    def get_document(self, collection_name, obj_id):
        doc_ref = self._db.collection(collection_name).document(obj_id)
        doc = doc_ref.get()
        docs_list = []

        # print(u'{} => {}'.format(doc.id, doc.to_dict()))
        docs_list.append(doc.to_dict())

        return docs_list

    def document_exists(self, collection_name, obj_id):
        doc_ref = self._db.collection(collection_name).document(obj_id)
        doc = doc_ref.get()

        return doc.exists

    # op can be '<', '<=', '==', '>', '>=', or u'array_contains'
    def get_multi_documents(self, collection_name, key, op, value):
        doc_ref = self._db.collection(collection_name).where(key, op, value)
        docs = doc_ref.get()
        docs_list = []

        for doc in docs:
            print(u'{} => {}'.format(doc.id, doc.to_dict()))
            docs_list.append(doc.to_dict())

        return docs_list

    def get_multi_documents_ids(self, collection_name, key, op, value):
        doc_ref = self._db.collection(collection_name).where(key, op, value)
        docs = doc_ref.get()
        id_list = []

        for doc in docs:
            # print(u'{} => {}'.format(doc.id, doc.to_dict()))
            id_list.append(doc.id)

        return id_list

    # nested_keys: a list of nested fields
    def get_multi_documents_by_nested_field(self, collection_name, nested_keys, op, value):
        keys = '.'.join(nested_keys)
        self.get_multi_documents(collection_name, keys, op, value)

    def update_field(self, collection_name, obj_id, key, new_value):
        doc_ref = self._db.collection(collection_name).document(obj_id)
        doc_ref.update({key: new_value})

    # nested_keys: a list of nested fields
    def update_nested_field(self, collection_name, obj_id, nested_keys, new_value):
        keys = '.'.join(nested_keys)
        self.update_field(collection_name, obj_id, keys, new_value)

    # key has to be a array field
    # new_value: a list of new elements
    def add_elements_to_array_field(self, collection_name, obj_id, key, new_elements):
        doc_ref = self._db.collection(collection_name).document(obj_id)
        doc_ref.update({key: ArrayUnion(new_elements)})

    def remove_elements_from_array_field(self, collection_name, obj_id, key, elements):
        doc_ref = self._db.collection(collection_name).document(obj_id)
        doc_ref.update({key: ArrayRemove(elements)})

    def refresh_attribute_collection(self, collection_name1, attribute, collection_name2):
        attribute_values = set()
        doc_ref = self._db.collection(collection_name1)
        docs = doc_ref.get()

        for doc in docs:
            value = doc.to_dict()[attribute]

            if type(value) is list:
                for element in value:
                    if ',' in element:
                        attribute_values = attribute_values.union(set(v.strip().lower() for v in element.split(',')))
                    else:
                        attribute_values.add(element.strip().lower())
            elif type(value) is str:
                attribute_values.add(value.strip().lower())
            else:
                attribute_values.add(value)

        attribute_values = attribute_values - {''}

        obj_id = collection_name1 + '_' + attribute
        obj = TutorAttributeFirestore(attribute_values)

        if self.document_exists(collection_name2, obj_id):
            self.delete_document(collection_name2, obj_id)

        self.save_document(collection_name2, obj, obj_id)

    # currently only support numerical value fields
    @firestore.transactional
    def update_field_in_transaction(self, collection_name, obj_id, key, new_value):
        doc_ref = self._db.collection(collection_name).document(obj_id)
        transaction = self._db.transaction()
        snapshot = doc_ref.get(transaction=transaction)

        transaction.update(doc_ref, {
            key: snapshot.get(key) + new_value
        })
