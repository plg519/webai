import json
from pprint import pprint
from collections import defaultdict


class LanguageUnderstander:


    @staticmethod
    def get_university_ranking(ranking_json_path, university_name):
        university_ranking_d = defaultdict(int)
        with open(ranking_json_path) as f:
            ranking_data = json.load(f)
        for items in ranking_data["data"]:
            university_ranking_d[items['name']] = items['rank']

        return university_ranking_d[university_name]

    @staticmethod
    def get_university_ranking_d(ranking_json_path):
        university_ranking_d = defaultdict(int)
        with open(ranking_json_path) as f:
            ranking_data = json.load(f)
        for items in ranking_data["data"]:
            university_ranking_d[items['name']] = items['rank']
        return university_ranking_d


ranking_json_path = r"/Users/plg519/WebAI_data/world_university_ranking_2018.json"
# university_name = r"University of Oxford"
# university_name = r"Stanford University"
# university_name = r"University of Utah"
# university_name = r"University of Kansas"
university_name = r"Princeton University"



# rank = LanguageUnderstander.get_university_ranking(ranking_json_path, university_name)
# print(rank)