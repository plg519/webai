import pyautogui
import time
import numpy as np
import cv2
from Classes.ColorImage import ColorImage
from Classes.RegionDetector import RegionDetector
from pytesseract import image_to_string



class VisionCrawler:

    num_sent_invitations = 0
    sent_enough_invitations = False
    screen_scrolled_to_top = False
    screen_scrolled_to_bottom = False

    # def __init__(self):
    #     pass

    @staticmethod
    def scroll_once(num_mouse_clicks):
        pyautogui.scroll(-1 * num_mouse_clicks)

    @staticmethod
    def space_once():
        pyautogui.press('pagedown')
        # pyautogui.press('space')

    @staticmethod
    def reach_end_page(previous_screenshot, current_screenshot):
        # print("screen1 shape: ", np.array(previous_screenshot).shape)
        # print("screen2 shape: ", np.array(current_screenshot).shape)
        if (np.array(previous_screenshot) == np.array(current_screenshot)).all():
            return True
        else:
            return False

    @staticmethod
    def go_to_next_page(current_screenshot):
        cv_im = np.array(current_screenshot)
        cv_im = cv2.cvtColor(cv_im, cv2.COLOR_RGB2BGR)
        test_im = ColorImage(pixel_values=cv_im)
        resized_im = test_im.resize(height=900)
        # resized_im.show()


        dilation_x = 3
        dilation_y = 2
        dilation_num_iterations = 5
        x_left_margin = 0.5
        x_right_margin = 0.75
        # y_top_margin = 0.5
        y_top_margin = 0.4
        y_bottom_margin = 0.75
        resized_im.detect_and_click_next_button(dilation_x, dilation_y, dilation_num_iterations,
                                                x_left_margin, x_right_margin, y_top_margin, y_bottom_margin)

    @classmethod
    def check_email_required(cls, screenshot):
        #resize image
        cv_im = np.array(screenshot)
        # print("cv_im.shape: ", cv_im.shape)
        # cv_im = cv2.cvtColor(cv_im, cv2.COLOR_BGR2RGB)
        cv_im = cv2.cvtColor(cv_im, cv2.COLOR_RGB2BGR)
        test_im = ColorImage(pixel_values=cv_im)
        # print("test_im", test_im.pixel_values.size)
        resized_im = test_im.resize(height=900)

        #skip if email required
        resized_im_cv = resized_im._pixel_values
        gray = cv2.cvtColor(resized_im_cv, cv2.COLOR_BGR2GRAY)
        # blurred = cv2.GaussianBlur(gray, (11, 11), 0)
        thresh = cv2.threshold(gray, 200, 255, cv2.THRESH_BINARY)[1]
        text = image_to_string(thresh)
        # print(text)
        if "emai" in text or "required" in text or "why" in text or "also" in text:
            # print("detected email")
            pyautogui.press('esc')
            time.sleep(0.5)
            return True
        return False


    @classmethod
    def add_note_and_send(cls, screenshot, note):
        #resize image
        cv_im = np.array(screenshot)
        # print("cv_im.shape: ", cv_im.shape)
        # cv_im = cv2.cvtColor(cv_im, cv2.COLOR_BGR2RGB)
        cv_im = cv2.cvtColor(cv_im, cv2.COLOR_RGB2BGR)
        test_im = ColorImage(pixel_values=cv_im)
        # print("test_im", test_im.pixel_values.size)
        resized_im = test_im.resize(height=900)

        #im.detect add note button and click
        dilation_x = 3
        dilation_y = 2
        dilation_num_iterations = 5
        resized_im.detect_and_click_add_a_note_button(dilation_x, dilation_y, dilation_num_iterations)

        # #add note
        resized_im.add_note(note)
        #detect send invitation

        #need to debug on click "send invitation" button
        time.sleep(1)
        screenshot_pil = pyautogui.screenshot()
        cv_im = np.array(screenshot_pil)
        # print("cv_im.shape: ", cv_im.shape)
        # cv_im = cv2.cvtColor(cv_im, cv2.COLOR_BGR2RGB)
        cv_im = cv2.cvtColor(cv_im, cv2.COLOR_RGB2BGR)
        test_im = ColorImage(pixel_values=cv_im)
        # print("test_im", test_im.pixel_values.size)
        resized_im = test_im.resize(height=900)
        # # resized_im.show()
        dilation_x = 3
        dilation_y = 2
        dilation_num_iterations = 5
        resized_im.detect_and_click_send_invitation_button(cls, dilation_x, dilation_y, dilation_num_iterations)
        # print("VisionCrawler status: ", cls.screen_scrolled_to_top, " after detect_and_click_send_invitation_button")



    # @staticmethod
    @classmethod
    def process_screenshot(cls, screenshot, keywords, wanted_num_invitations):
        cv_im = np.array(screenshot)

        # print("cv_im.shape: ", cv_im.shape)

        # cv_im = cv2.cvtColor(cv_im, cv2.COLOR_BGR2RGB)
        cv_im = cv2.cvtColor(cv_im, cv2.COLOR_RGB2BGR)

        test_im = ColorImage(pixel_values=cv_im)

        # print("test_im", test_im.pixel_values.size)

        resized_im = test_im.resize(height=900)
        # resized_im = test_im
        # print("resized_im", resized_im.pixel_values)

        # resized_im.show()

        # detector = RegionDetector()


        dilation_x = 2
        dilation_y = 3
        dilation_num_iterations = 5
        side_margin_ratio = 0.2

        # detected_regions = detector.detect_regions(resized_im, dilation_x, dilation_y, dilation_num_iterations)
        # detected_regions = detector.detect_circles(resized_im)
        # detected_regions = detector.detect_linkedin_connect(resized_im, dilation_x, dilation_y, dilation_num_iterations, side_margin)

        # detected_regions = RegionDetector.detect_profile_photos(resized_im, side_margin)
        profile_regions = RegionDetector.detect_profile_photos(resized_im, side_margin_ratio)

        # left_margin_ratio = 0.05
        # right_margin_ratio = 0.25
        # profile_regions = RegionDetector.detect_search_profile_photos(resized_im, left_margin_ratio, right_margin_ratio)


        # for region in profile_regions:
        #     region.show()

        connect_regions = RegionDetector.detect_linkedin_connect(resized_im, dilation_x, dilation_y,
                                                                 dilation_num_iterations, side_margin_ratio)

        # for region in connect_regions:
        #     region.show()
        #     print(region._x, " ", region._y)

        resized_im.add_detected_regions(profile_regions)
        resized_im.add_detected_regions(connect_regions)

        # v_thresh = 215
        # h_thresh = 20

        v_thresh = 255
        h_thresh = 40

        resized_im.merge_photo_connect_to_profile(v_thresh, h_thresh)

        top_region_cate = "profile_photo"
        bottom_region_cate = "connect_button"
        resized_im.add_photo_connect_middle_subregion(top_region_cate, bottom_region_cate)

        resized_im.add_positions_connections_subregions()

        resized_im.ocr_position_regions()

        category = "profile"

        resized_im.draw_detected_regions_with_subregions(category)
        resized_im.show()
        time.sleep(80)

        resized_im.connect(cls, keywords, wanted_num_invitations)


    @classmethod
    def process_search_screenshot(cls, screenshot, wanted_num_invitations, general_note_message):

        # if cls.screen_scrolled_to_top == True:
        #     # screenshot = pyautogui.screenshot()
        #     return

        cv_im = np.array(screenshot)

        # print("cv_im.shape: ", cv_im.shape)

        # cv_im = cv2.cvtColor(cv_im, cv2.COLOR_BGR2RGB)
        cv_im = cv2.cvtColor(cv_im, cv2.COLOR_RGB2BGR)

        test_im = ColorImage(pixel_values=cv_im)

        # print("test_im", test_im.pixel_values.size)

        resized_im = test_im.resize(height=900)
        # resized_im = test_im
        # print("resized_im", resized_im.pixel_values)

        # resized_im.show()

        # detector = RegionDetector()


        dilation_x = 2
        dilation_y = 3
        dilation_num_iterations = 5
        side_margin_ratio = 0.2

        # detected_regions = detector.detect_regions(resized_im, dilation_x, dilation_y, dilation_num_iterations)
        # detected_regions = detector.detect_circles(resized_im)
        # detected_regions = detector.detect_linkedin_connect(resized_im, dilation_x, dilation_y, dilation_num_iterations, side_margin)

        # detected_regions = RegionDetector.detect_profile_photos(resized_im, side_margin)
        # profile_regions = RegionDetector.detect_profile_photos(resized_im, side_margin_ratio)

        left_margin_ratio = 0.05
        # right_margin_ratio = 0.25
        right_margin_ratio = 0.66

        profile_regions = RegionDetector.detect_search_profile_photos(resized_im, left_margin_ratio, right_margin_ratio)


        # for region in profile_regions:
        #     region.show()

        connect_left_margin_ratio = 0.5
        connect_right_margin_ratio = 0.8
        # connect_regions = RegionDetector.detect_linkedin_connect(resized_im, dilation_x, dilation_y,
        #                                                          dilation_num_iterations, side_margin_ratio)
        connect_regions = RegionDetector.detect_linkedin_connect(resized_im, dilation_x, dilation_y,
                                                                 dilation_num_iterations, connect_left_margin_ratio, connect_right_margin_ratio)

        # for region in connect_regions:
        #     region.show()
        #     print(region._x, " ", region._y)

        resized_im.add_detected_regions(profile_regions)
        resized_im.add_detected_regions(connect_regions)

        # v_thresh = 215
        # h_thresh = 20

        # v_thresh = 10
        v_thresh = 20
        h_thresh = 800

        resized_im.merge_search_photo_connect_to_profile(v_thresh, h_thresh)

        left_region_cate = "profile_photo"
        right_region_cate = "connect_button"
        resized_im.add_search_photo_connect_middle_subregion(left_region_cate, right_region_cate)

        # resized_im.add_positions_connections_subregions()
        resized_im.add_name_connections_subregions()

        # resized_im.ocr_position_regions()
        resized_im.ocr_name_regions()


        category = "profile"

        # resized_im.draw_detected_regions_with_subregions(category)
        # resized_im.show()
        # time.sleep(80)

        # resized_im.connect(cls, keywords, wanted_num_invitations)
        resized_im.connect_search(cls, wanted_num_invitations, general_note_message)
        # print("VisionCrawler status: ", VisionCrawler.screen_scrolled_to_top,
        #       " after resized_im.connect_search")

    @classmethod
    def process_ace_screenshot(cls, screenshot):
        cv_im = np.array(screenshot)

        # print("cv_im.shape: ", cv_im.shape)

        # cv_im = cv2.cvtColor(cv_im, cv2.COLOR_BGR2RGB)
        cv_im = cv2.cvtColor(cv_im, cv2.COLOR_RGB2BGR)

        test_im = ColorImage(pixel_values=cv_im)

        # print("test_im", test_im.pixel_values.size)

        resized_im = test_im.resize(height=900)
        # resized_im = test_im
        # print("resized_im", resized_im.pixel_values)

        # resized_im.show()

        # detector = RegionDetector()


        dilation_x = 2
        dilation_y = 3
        dilation_num_iterations = 5
        side_margin_ratio = 0.2

        # detected_regions = detector.detect_regions(resized_im, dilation_x, dilation_y, dilation_num_iterations)
        # detected_regions = detector.detect_circles(resized_im)
        # detected_regions = detector.detect_linkedin_connect(resized_im, dilation_x, dilation_y, dilation_num_iterations, side_margin)

        # detected_regions = RegionDetector.detect_profile_photos(resized_im, side_margin)
        profile_regions = RegionDetector.detect_ace_profile_regions(resized_im, side_margin_ratio)

        # # left_margin_ratio = 0.05
        # # right_margin_ratio = 0.25
        # # profile_regions = RegionDetector.detect_search_profile_photos(resized_im, left_margin_ratio, right_margin_ratio)
        #
        #
        for region in profile_regions:
            region.show()
        #
        # connect_regions = RegionDetector.detect_linkedin_connect(resized_im, dilation_x, dilation_y,
        #                                                          dilation_num_iterations, side_margin_ratio)
        #
        # # for region in connect_regions:
        # #     region.show()
        # #     print(region._x, " ", region._y)
        #
        # resized_im.add_detected_regions(profile_regions)
        # resized_im.add_detected_regions(connect_regions)
        #
        # # v_thresh = 215
        # # h_thresh = 20
        #
        # v_thresh = 255
        # h_thresh = 40
        #
        # resized_im.merge_photo_connect_to_profile(v_thresh, h_thresh)
        #
        # top_region_cate = "profile_photo"
        # bottom_region_cate = "connect_button"
        # resized_im.add_photo_connect_middle_subregion(top_region_cate, bottom_region_cate)
        #
        # resized_im.add_positions_connections_subregions()
        #
        # resized_im.ocr_position_regions()
        #
        # category = "profile"
        #
        # resized_im.draw_detected_regions_with_subregions(category)
        # resized_im.show()
        # time.sleep(80)


    @staticmethod
    def keep_scrolling_and_detecting(pause_time):
        # for i in range(num_scrolls):
        while (True):
            # try:
                # VisionCrawler.scroll_once(num_clicks_each_time)
                screenshot_pil = pyautogui.screenshot()
                # print("screenshot: ", screenshot_pil)
                VisionCrawler.process_ace_screenshot(screenshot_pil)

                VisionCrawler.space_once()
                time.sleep(pause_time)

            # except Exception as exception:
            #     print(exception)

    @staticmethod
    def keep_scrolling_and_connecting(pause_time, keywords, wanted_num_invitations):
        # for i in range(num_scrolls):
        while(True):
            try:
                # VisionCrawler.scroll_once(num_clicks_each_time)
                screenshot_pil = pyautogui.screenshot()
                # print("screenshot: ", screenshot_pil)
                VisionCrawler.process_screenshot(screenshot_pil, keywords, wanted_num_invitations)

                if VisionCrawler.sent_enough_invitations:
                    break

                VisionCrawler.space_once()
                time.sleep(pause_time)

                #stop after sending targeted number of invitations

                break

            except Exception as exception:
                print(exception)




    @staticmethod
    # def keep_search_scrolling_and_connecting(pause_time, keywords, wanted_num_invitations, general_note_message):
    def keep_search_scrolling_and_connecting(pause_time, wanted_num_invitations, general_note_message):
        # for i in range(num_scrolls):
        while(True):
            # try:
                # VisionCrawler.scroll_once(num_clicks_each_time)
                screenshot_pil = pyautogui.screenshot()
                VisionCrawler.screen_scrolled_to_top = False
                VisionCrawler.screen_scrolled_to_bottom == False
                # print("screenshot 1")
                # print("screenshot: ", screenshot_pil)
                # VisionCrawler.process_screenshot(screenshot_pil, keywords, wanted_num_invitations, general_note_message) #for recommend
                # VisionCrawler.process_search_screenshot(screenshot_pil, keywords, wanted_num_invitations, general_note_message) #for recommend
                VisionCrawler.process_search_screenshot(screenshot_pil, wanted_num_invitations, general_note_message)
                # print("VisionCrawler status: ", VisionCrawler.screen_scrolled_to_top,
                #   " after VisionCrawler.process_search_screenshot")


                if VisionCrawler.sent_enough_invitations:
                    break

                if VisionCrawler.screen_scrolled_to_top == True:
                    continue

                if VisionCrawler.screen_scrolled_to_bottom == True:
                    continue

                time.sleep(pause_time)
                VisionCrawler.space_once()
                # print("next screen")
                time.sleep(pause_time)
                current_screenshot = pyautogui.screenshot()
                # print("screenshot 2")
                if VisionCrawler.reach_end_page(screenshot_pil, current_screenshot):
                    #click next button
                    # print("reached page end")
                    VisionCrawler.go_to_next_page(current_screenshot)

                #stop after sending targeted number of invitations

                # break

            # except Exception as exception:
            #     print(exception)

    @staticmethod
    def pause(num_sceonds):
        time.sleep(num_sceonds)

    @classmethod
    def increment_num_invitations_by_one(cls):
        cls.num_sent_invitations += 1

    @classmethod
    def mark_enough_invitations(cls):
        cls.sent_enough_invitations = True