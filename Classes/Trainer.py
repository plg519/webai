import pandas as pd
from sklearn import linear_model
import random
# import matplotlib.pyplot as plt
import math
import pickle


class Trainer:

    @staticmethod
    def convert_to_df_write_to_csv(tutors, csv_path):
        df = pd.DataFrame.from_records(tutor.to_dict() for tutor in tutors)
        # print("df: ", df)
        # print("df.columns: ", df.columns)
        df.to_csv(csv_path)

    @staticmethod
    def split_data(a_list, test_raio, set_seed = 1, random_split = True,):
        if set_seed:
            seed = 5
        else:
            seed = random.randint(0, 9)

        if random_split:
            random.Random(seed).shuffle(a_list)

        num_test_instances = int(test_raio * len(a_list))
        train_data = a_list[:-num_test_instances]
        test_data = a_list[-num_test_instances:]
        return train_data, test_data


    @staticmethod
    def train(tutors, save_model_path):
        df = pd.DataFrame.from_records(tutor.to_dict() for tutor in tutors)
        # print("df: ", df)
        # print("df.columns: ", df.columns)
        # X = df[["age", "beauty_score", "gender", "num_missing_fields", "num_words_in_description", "num_words_in_summary","relative_w", "relative_h", "relative_x", "relative_y", "tutoring_years", "university_ranking" ]]
        X = df[["age", "beauty_score", "gender", "num_missing_fields", "num_words_in_description", "num_words_in_summary","relative_w", "relative_h", "relative_x", "relative_y", "tutoring_years", "university_ranking"]].values
        # y = df[["rating"]]
        y = df["rating"].values
        lm = linear_model.LinearRegression()
        # print("X: ", X)
        # print("y: ", y)
        model = lm.fit(X, y)

        with open(save_model_path, 'wb') as f:
            pickle.dump(model, f)

        # predictions = lm.predict(X)
        #
        # print("predictions: \n")
        # print(predictions)
        #
        # print("score: \n")
        # print(lm.score(X, y))
        #
        # print("coeffe")
        # print(lm.coef_)
        return lm

    @staticmethod
    def predict(tutors, model):
        df = pd.DataFrame.from_records(tutor.to_dict() for tutor in tutors)
        X = df[["age", "beauty_score", "gender", "num_missing_fields", "num_words_in_description", "num_words_in_summary","relative_w", "relative_h", "relative_x", "relative_y", "tutoring_years", "university_ranking"]].values
        predictions = model.predict(X)
        return predictions

    @staticmethod
    def get_labels(tutors):
        df = pd.DataFrame.from_records(tutor.to_dict() for tutor in tutors)
        y = df["rating"].values
        return y


    # @staticmethod
    # def save_prediction_true_plot(true_labels, predicted_labels, save_figure_path, rmse):
    #     plt.figure()
    #     # print("rmse in plot title: ", "{:.2f}".format(rmse))
    #     plt.title("y_pred vs y_true, rmse: " + "{:.2f}".format(rmse))
    #     plt.xlabel("true ratings")
    #     plt.ylabel("predicted ratings")
    #     plt.xlim(0, 16)
    #     plt.ylim(0, 16)
    #     plt.plot(true_labels, predicted_labels, "ro")
    #     plt.savefig(save_figure_path)

    @staticmethod
    def save_pred_true_to_csv(true_labels, predicted_labels, save_path):
        true_pred_df = pd.DataFrame({"true_ratings": true_labels, "predicted_ratings": predicted_labels})
        true_pred_df.to_csv(save_path)


    @staticmethod
    def compute_RMSE(true_labels, predicted_labels):
        print("true_labels: ", true_labels)
        print("predicted_labels: ", predicted_labels)
        sse = 0
        num_instances = len(true_labels)
        for i in range(num_instances):
            sse += (true_labels[i] - predicted_labels[i]) ** 2

        mse = sse/float(num_instances)
        rmse = math.sqrt(mse)
        return rmse



