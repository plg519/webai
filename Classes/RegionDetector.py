from cv2 import cvtColor, threshold, bitwise_and, COLOR_BGR2GRAY, THRESH_BINARY, getStructuringElement, THRESH_BINARY_INV, MORPH_CROSS, dilate, findContours, RETR_EXTERNAL, CHAIN_APPROX_NONE, boundingRect, imshow, waitKey
from cv2 import boundingRect
# from dependencies.cv2.cv2 import boundingRect, cvtColor, threshold, bitwise_and, COLOR_BGR2GRAY, THRESH_BINARY, getStructuringElement, THRESH_BINARY_INV, MORPH_CROSS, dilate, findContours, RETR_EXTERNAL, CHAIN_APPROX_NONE


from Classes.RectangleRegion import RectangleRegion
# from dependencies.numpy.core.numeric import asarray


class RegionDetector():

    # def __init__(self):
    #     return

    @staticmethod
    def detect_regions(color_image,  dilation_x, dilation_y, dilation_num_iterations):
        detected_regions = []
        # img = imread(file_name)
        # img_final = imread(file_name)

        img = color_image.pixel_values

        img_final = img.copy()

        img2gray = cvtColor(img, COLOR_BGR2GRAY)
        ret, mask = threshold(img2gray, 180, 255, THRESH_BINARY)
        image_final = bitwise_and(img2gray, img2gray, mask=mask)
        ret, new_img = threshold(image_final, 180, 255,
                                     THRESH_BINARY_INV)  # for black text , cv.THRESH_BINARY_INV
        '''
                line  8 to 12  : Remove noisy portion 
        '''
        kernel = getStructuringElement(MORPH_CROSS, (dilation_x,
                                                             dilation_y))  # to manipulate the orientation of dilution , large x means horizonatally dilating  more, large y means vertically dilating more
        dilated = dilate(new_img, kernel,
                             iterations=dilation_num_iterations)  # dilate , more the iteration more the dilation

        # for x.x

        _, contours, hierarchy = findContours(dilated, RETR_EXTERNAL,
                                                  CHAIN_APPROX_NONE)  # findContours returns 3 variables for getting contours

        # for cv3.x.x comment above line and uncomment line below

        # image, contours, hierarchy = findContours(dilated,RETR_EXTERNAL,CHAIN_APPROX_NONE)


        for contour in contours:
            # get rectangle bounding contour
            [x, y, w, h] = boundingRect(contour)

            # Don't plot small false positives that aren't text
            if w < 35 and h < 35:
                continue

            detected_regions.append(RectangleRegion(x, y, w, h, pixel_values = img_final[y:y+h, x:x+w]))

        return detected_regions



    @staticmethod
    def detect_ace_profile_regions(color_image,  dilation_x, dilation_y, dilation_num_iterations):
        detected_regions = []
        # img = imread(file_name)
        # img_final = imread(file_name)

        img = color_image.pixel_values

        img_final = img.copy()

        # print("img: ", img)
        # print("type(img) ", type(img))
        # print("img.shape: ", img.shape)
        # imshow("img: ", asarray(img))
        # waitKey()


        img2gray = cvtColor(img, COLOR_BGR2GRAY)
        ret, mask = threshold(img2gray, 180, 255, THRESH_BINARY)
        image_final = bitwise_and(img2gray, img2gray, mask=mask)
        ret, new_img = threshold(image_final, 180, 255,
                                     THRESH_BINARY_INV)  # for black text , cv.THRESH_BINARY_INV
        '''
                line  8 to 12  : Remove noisy portion 
        '''
        kernel = getStructuringElement(MORPH_CROSS, (dilation_x,
                                                             dilation_y))  # to manipulate the orientation of dilution , large x means horizonatally dilating  more, large y means vertically dilating more
        dilated = dilate(new_img, kernel,
                             iterations=dilation_num_iterations)  # dilate , more the iteration more the dilation

        # for x.x

        _, contours, hierarchy = findContours(dilated, RETR_EXTERNAL,
                                                  CHAIN_APPROX_NONE)  # findContours returns 3 variables for getting contours

        # for cv3.x.x comment above line and uncomment line below

        # image, contours, hierarchy = findContours(dilated,RETR_EXTERNAL,CHAIN_APPROX_NONE)


        for contour in contours:
            # get rectangle bounding contour
            [x, y, w, h] = boundingRect(contour)

            # Don't plot small false positives that aren't text
            if w < 35 and h < 35:
                continue

            # rectangle(img, (x,y), (x + w, y + h), (255, 0, 255), 2)

            # detected_regions.append(RectangleRegion(x, y, w, h, pixel_values = img_final[y:y+h, x:x+w]))
            detected_regions.append(RectangleRegion(x, y, w, h, pixel_values=img_final[y-5:y + (h+5), x:x + w]))

        # imshow("result: ", img)
        # waitKey()
        return detected_regions






