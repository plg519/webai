from selenium import webdriver
from os import getcwd
from os import path
from time import sleep

PHANTOMJS_EXECUTABLE_PATH = r"/Users/plg519/node_modules/phantomjs/lib/phantom/bin/phantomjs"

def save_screenshot(url, pause_duration = 10, save_path = path.join(getcwd(), "test.png")):
    driver = webdriver.PhantomJS(PHANTOMJS_EXECUTABLE_PATH)
    driver.get(url)
    sleep(pause_duration)
    driver.save_screenshot(save_path)


def save_screenshots(urls, save_folder, pause_duration = 20.0):
    for url in urls:
        save_path = path.join(save_folder, url.strip().split("/")[-1] + ".png")
        print("save_path: ", save_path)
        save_screenshot(url, pause_duration, save_path)



# # url = r"http://www.yahoo.com"
# url = r"https://www.acetutor.io/prahladmenon"

# url = r"https://www.acetutor.io/ace/mohyislam"
# url = r"https://www.acetutor.io/sophiatsai"
# url = r"https://www.acetutor.io/ace/sophiatsai"

#
# save_screenshot(url, pause_duration = 20)