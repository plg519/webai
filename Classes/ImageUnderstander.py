# import dlib
import cv2
import numpy as np
from Classes.RectangleRegion import RectangleRegion
# import lycon

class ImageUnderstander:

    def __init__(self):
        self._face_detector = None
        self._age_gender_predictor = None
        self._beauty_predictor = None

    @property
    def face_detector(self):
        return self._face_detector

    @face_detector.setter
    def face_detector(self, new_face_detector):
        self._face_detector = new_face_detector

    @property
    def age_gender_predictor(self):
        return self._age_gender_predictor

    @age_gender_predictor.setter
    def age_gender_predictor(self, new_age_gender_predictor):
        self._age_gender_predictor = new_age_gender_predictor

    @property
    def beauty_predictor(self):
        return self._beauty_predictor

    @beauty_predictor.setter
    def beauty_predictor(self, new_beauty_predictor):
        self._beauty_predictor = new_beauty_predictor

    def init_face_detector(self, face_detector_model_path):
        # detector = dlib.get_frontal_face_detector()
        cascade_detector = cv2.CascadeClassifier(face_detector_model_path)
        # self.face_detector = detector
        self.face_detector = cascade_detector

    @staticmethod
    def set_detected_faces(detector, profile_photo, margin = 0.3, num_faces = 1):
        cv_im = profile_photo.pixel_values
        input_img = cv2.cvtColor(cv_im, cv2.COLOR_BGR2RGB)
        img_h, img_w, _ = np.shape(input_img)

        # detect faces using dlib detector
        # detected = detector(input_img, 1)



        # image = cv2.imread(imagePath)
        gray = cv2.cvtColor(input_img, cv2.COLOR_BGR2GRAY)

        detected = detector.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            # minSize=(30, 30),
            minSize=(15, 15),
            flags=cv2.CASCADE_SCALE_IMAGE

        )


        # detected = detector(input_img, 3)
        # detected = detector(input_img, 3)


        first_n_detected = detected[:num_faces]
        face_regions = []
        # if len(first_n_detected) > 0:
        #     for i, d in enumerate(first_n_detected):
        #         x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
        #         xw1 = max(int(x1 - margin * w), 0)
        #         yw1 = max(int(y1 - margin * h - 10), 0)
        #         xw2 = min(int(x2 + margin * w), img_w - 1)
        #         yw2 = min(int(y2 + margin * h), img_h - 1)
        #         # cv2.rectangle(cv_im, (x1, y1), (x2, y2), (255, 0, 0), 2)
        #         # cv2.rectangle(img, (xw1, yw1), (xw2, yw2), (255, 0, 0), 2)
        #         # faces[i, :, :, :] = cv2.resize(cv_im[yw1:yw2 + 1, xw1:xw2 + 1, :], (img_size, img_size))
        #         face_region = RectangleRegion(x=xw1, y=yw1, w=xw2-xw1, h=yw2-yw1,
        #                                       pixel_values=cv_im[yw1:yw2 + 1, xw1:xw2 + 1, :],
        #                                       region_type = "face_region")
        #         face_regions.append(face_region)

        if len(first_n_detected) > 0:
            for (x1, y1, w, h) in first_n_detected:
                x2 = x1 + w
                y2 = y1 + h
                xw1 = max(int(x1 - margin * w), 0)
                yw1 = max(int(y1 - margin * h - 10), 0)
                xw2 = min(int(x2 + margin * w), img_w - 1)
                yw2 = min(int(y2 + margin * h), img_h - 1)
                # cv2.rectangle(cv_im, (x1, y1), (x2, y2), (255, 0, 0), 2)
                # cv2.rectangle(img, (xw1, yw1), (xw2, yw2), (255, 0, 0), 2)
                # faces[i, :, :, :] = cv2.resize(cv_im[yw1:yw2 + 1, xw1:xw2 + 1, :], (img_size, img_size))
                face_region = RectangleRegion(x=xw1, y=yw1, w=xw2-xw1, h=yw2-yw1,
                                              pixel_values=cv_im[yw1:yw2 + 1, xw1:xw2 + 1, :],
                                              region_type = "face_region")
                face_regions.append(face_region)

        profile_photo.add_sub_regions(face_regions)
        # profile_photo.draw_detected_regions_with_type("face")

    @staticmethod
    def compute_age_gender(model, profile_photo, img_size):

        num_faces = len(profile_photo.sub_regions)

        if num_faces < 1:
            return -1, -1

        faces = np.empty((num_faces, img_size, img_size, 3))

        for i in range(num_faces):
            faces[i, :, :, :] = cv2.resize(profile_photo.sub_regions[i].pixel_values, (img_size, img_size))
        results = model.predict(faces)

        # print("results: ", results)
        predicted_genders = results[0]
        ages = np.arange(0, 101).reshape(101, 1)
        predicted_ages = results[1].dot(ages).flatten()

        predicted_age = int(predicted_ages[0])
        predicted_gender = int(predicted_genders[i][0] < 0.5)

        return predicted_age, predicted_gender

    @staticmethod
    def compute_beauty_score(model, profile_photo, img_width, img_height):
        num_faces = len(profile_photo.sub_regions)

        if num_faces < 1:
            return -1

        face_cv_im = profile_photo.sub_regions[0].pixel_values
        # reized_photo = lycon.resize(face_cv_im, width=img_width, height=img_height, interpolation=lycon.Interpolation.CUBIC)
        reized_photo = cv2.resize(face_cv_im, (img_width, img_height), interpolation=cv2.INTER_CUBIC)
        img = np.expand_dims(reized_photo, axis=0)
        prediction = model.predict(img)
        return float(prediction[0][0])

    @staticmethod
    def compute_professional_score(model, profile_photo, img_width, img_height):
        cv_im = profile_photo.pixel_values
        # reized_photo = lycon.resize(cv_im, width=img_width, height=img_height, interpolation=lycon.Interpolation.CUBIC)
        reized_photo = cv2.resize(cv_im, (img_width, img_height), interpolation=cv2.INTER_CUBIC)
        img = np.expand_dims(reized_photo, axis=0)
        prediction = model.predict(img)
        # print("prediction: ", prediction)

        # prob = model.predict_proba(img)
        # print("prob: ", prob)

        return 1 - float(prediction[0][0])


    @staticmethod
    def compute_face_position(profile_photo):
        num_faces = len(profile_photo.sub_regions)

        if num_faces < 1:
            return 0, 0, 0, 0

        ori_x1, ori_y1, ori_w, ori_h = profile_photo.x, profile_photo.y, profile_photo.w, profile_photo.h
        face_x1, face_y1, face_w, face_h = profile_photo.sub_regions[0].x, profile_photo.sub_regions[0].y, profile_photo.sub_regions[0].w, profile_photo.sub_regions[0].h

        return face_x1/float(ori_w), face_y1/float(ori_h), face_w/float(ori_w), face_h/float(ori_h)




