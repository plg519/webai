import spacy
from spacy.matcher import Matcher, PhraseMatcher
from Classes.FirestoreHandler import FirestoreHandler
import re
from collections import defaultdict
from nltk.corpus import wordnet

class NameEntityDetector:

    def __init__(self, categorical_attribute_values_dict, continuous_attribute_keywords_dict):
        self.nlp = spacy.load('en_core_web_sm')
        # self.matcher = Matcher(self.nlp.vocab)
        self.phrase_matcher = PhraseMatcher(self.nlp.vocab)
        self.categorical_attribute_values_dict = categorical_attribute_values_dict
        self.continuous_attribute_keywords_dict = continuous_attribute_keywords_dict


    def build_attribute_word_dicts(self, doc):
        categorical_entity_word_dict = defaultdict(list)
        continuous_entity_word_dict = defaultdict(list)

        #key: attribute (e.g. subjects), value: matched word from doc (e.g. "math")

        for attribute, values in self.categorical_attribute_values_dict.items():
            # for value in values:
                # pattern = [{'LOWER': value}]
            pattern =[self.nlp.make_doc(value) for value in values if len(self.nlp.make_doc(value)) < 15]
            self.phrase_matcher.add(attribute, None, *pattern)

        doc = self.nlp(doc.text) #must be after phrase_match.add()
        # print("doc: ", doc)

        matches = self.phrase_matcher(doc)

        # print("len(matches): ", len(matches))

        for match_id, start, end in matches:
            # print("match_id: ", match_id)
            string_id = self.nlp.vocab.strings[match_id]
            span = doc[start: end]
            categorical_entity_word_dict[string_id].append(span.text)



        self.phrase_matcher = PhraseMatcher(self.nlp.vocab)

        for attribute, values in self.continuous_attribute_keywords_dict.items():
            # for value in values:
                # pattern = [{'LOWER': value}]
            pattern =[self.nlp.make_doc(value) for value in values if len(self.nlp.make_doc(value)) < 10]
            self.phrase_matcher.add(attribute, None, *pattern)

        doc = self.nlp(doc.text) #must be after phrase_match.add()
        # print("doc: ", doc)

        matches = self.phrase_matcher(doc)

        # print("len(matches): ", len(matches))

        for match_id, start, end in matches:
            # print("match_id: ", match_id)
            string_id = self.nlp.vocab.strings[match_id]
            span = doc[start: end]
            previsou_span = doc[start - 1: start] #to add handle start=1 case
            # continuous_entity_word_dict[string_id].append(span.text)
            continuous_entity_word_dict[string_id].append(previsou_span.text)

        return categorical_entity_word_dict, continuous_entity_word_dict


    def get_synonyms(self, word):
        synonyms = set()
        for syn in wordnet.synsets(word):
            for l in syn.lemmas():
                synonyms.add(l.name())
        return synonyms



    def build_synonym_word_dict(self):
        #key: synonmy; value: original_word
        synonym_origins_dict = defaultdict(str)
        for attribute, values in self.categorical_attribute_values_dict.items():
            for value in values:
                # print("value: ", value)
                try:
                    synonyms = self.get_synonyms(value)
                    for synonym in synonyms:
                        synonym_origins_dict[synonym] = value
                    synonym_origins_dict[value] = value #add self as one of synonyms as well
                except Exception as e:
                    print(e)
                    continue
        return synonym_origins_dict


    def build_attribute_word_dicts_with_synonyms(self, doc, synonym_orgin_dict):
        categorical_entity_word_dict = defaultdict(list)
        continuous_entity_word_dict = defaultdict(list)

        #key: attribute (e.g. subjects), value: matched word from doc (e.g. "math")

        for attribute, values in self.categorical_attribute_values_dict.items():
            # for value in values:
                # pattern = [{'LOWER': value}]
            for value in values:
                synonyms = self.get_synonyms(value)
                # if value == "new york, ny":
                    # print("value: ", value, " synonyms: ", synonyms)
                pattern =[self.nlp.make_doc(synonym) for synonym in synonyms if len(self.nlp.make_doc(synonym)) < 15]
                pattern.append(self.nlp.make_doc(value)) #
                self.phrase_matcher.add(attribute, None, *pattern)

        doc = self.nlp(doc.text) #must be after phrase_match.add()
        # print("doc: ", doc)

        matches = self.phrase_matcher(doc)

        # print("len(matches): ", len(matches))

        for match_id, start, end in matches:
            # print("match_id: ", match_id)
            string_id = self.nlp.vocab.strings[match_id]
            span = doc[start: end]
            # categorical_entity_word_dict[string_id].append(span.text)
            # if synonym_orgin_dict[span.text] == 1:
            #     print("span.text.isdigit(): ", span.text.isdigit())
            if not synonym_orgin_dict[span.text].isdigit(): #not add numbers since categorical values could not be only number (e.g. 1 from ORC VisionCrawler)
                categorical_entity_word_dict[string_id].append(synonym_orgin_dict[span.text]) #key is original word based on matching synonym
            # if span.text == "stanford":
            #     print("categorical_entity_word_dict[span.text]: ", synonym_orgin_dict[span.text])


        self.phrase_matcher = PhraseMatcher(self.nlp.vocab)

        for attribute, values in self.continuous_attribute_keywords_dict.items():
            # for value in values:
                # pattern = [{'LOWER': value}]
            pattern =[self.nlp.make_doc(value) for value in values if len(self.nlp.make_doc(value)) < 10]
            self.phrase_matcher.add(attribute, None, *pattern)

        doc = self.nlp(doc.text) #must be after phrase_match.add()
        # print("doc: ", doc)

        matches = self.phrase_matcher(doc)

        # print("len(matches): ", len(matches))

        for match_id, start, end in matches:
            # print("match_id: ", match_id)
            string_id = self.nlp.vocab.strings[match_id]
            span = doc[start: end]
            previsou_span = doc[start - 1: start] #to add handle start=1 case
            # continuous_entity_word_dict[string_id].append(span.text)
            continuous_entity_word_dict[string_id].append(previsou_span.text)

        return categorical_entity_word_dict, continuous_entity_word_dict
