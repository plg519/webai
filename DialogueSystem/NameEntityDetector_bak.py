import spacy
from spacy.matcher import Matcher, PhraseMatcher
from Classes.FirestoreHandler import FirestoreHandler
import re
from collections import defaultdict

class NameEntityDetector:

    def __init__(self, categorical_attribute_values_dict, continuous_attribute_keywords_dict):
        self.nlp = spacy.load('en_core_web_sm')
        # self.matcher = Matcher(self.nlp.vocab)
        self.phrase_matcher = PhraseMatcher(self.nlp.vocab)
        self.categorical_attribute_values_dict = categorical_attribute_values_dict
        self.continuous_attribute_keywords_dict = continuous_attribute_keywords_dict


    def build_attribute_word_dict(self, doc):
        nlp = spacy.load('en_core_web_sm')
        #
        entity_word_dict = defaultdict(list)
        #
        # #key: attribute (e.g. subjects), value: matched word from doc (e.g. "math")
        # # for attribute, values in self.categorical_attribute_values_dict.items():
        # #     # for value in values:
        # #         # pattern = [{'LOWER': value}]
        # #     # pattern =[self.nlp.make_doc(value) for value in values if len(self.nlp.make_doc(value)) < 10]
        # #     pattern = []
        # #     for value in values:
        # #         phrase = self.nlp.make_doc(value)
        # #         print("phrase: ", phrase)
        # #         print(len(self.nlp.make_doc(value)))
        # #         if len(self.nlp.make_doc(value)) <= 10:
        # #             pattern.append(phrase)
        # #     # self.matcher.add(attribute, None, pattern)
        # #     print("pattern: ", pattern)
        # #     self.phraseMatcher.add(attribute, None, *pattern)
        #
        phrase_matcher = PhraseMatcher(nlp.vocab)
        #
        experience_list = ['years experience']
        # # exp_pattern = [self.nlp.make_doc(text) for text in experience_list]
        exp_pattern = [nlp.make_doc(text) for text in experience_list]
        #
        # self.phrase_matcher.add("experience:", None, *exp_pattern)
        #
        phrase_matcher.add("experience:", None, *exp_pattern)
        #
        print("doc: ", doc)
        #
        # # matches = self.matcher(doc)
        # # matches = self.phraseMatcher(doc)
        # matches = phrase_matcher(doc)
        #
        # print("len(matches): ", len(matches))
        #
        # for match_id, start, end in matches:
        #     print("match_id: ", match_id)
        #     string_id = self.nlp.vocab.strings[match_id]
        #     span = doc[start: end]
        #     entity_word_dict[string_id].append(span.text)
        #
        #
        # return entity_word_dict




        doc2 = nlp(
            u'I need a professional piano tutor with at least 5 years experience in Plano area. better graduated from harvard')

        print('doc2: ', doc2)
        print('doc: ', doc)

        doc = nlp(doc.text)

        matches = phrase_matcher(doc)
        # matches = self.phrase_matcher(doc)

        print("len(matches): ", len(matches))

        for match_id, start, end in matches:
            print("match_id: ", match_id)
            span = doc[start: end]
            print("span.text: ", span.text)






        #to-do: refactor above code use PhraseMatch; finish matches for continous attributes, add operator and convert to query