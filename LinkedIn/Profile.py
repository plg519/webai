from Classes.RectangleRegion import RectangleRegion

class LinkedInProfile():
    def __init__(self, profile_photo, connect_button, title = None):
        self._profile_photo = profile_photo
        self._connect_button = connect_button
        self._title = title

    @property
    def profile_photo(self):
        return self._profile_photo

    @profile_photo.setter
    def profile_photo(self, new_profile_photo):
        self._profile_photo = new_profile_photo

    @property
    def connect_button(self):
        return self._connect_button

    @connect_button.setter
    def connect_button(self, new_connect_button):
        self._connect_button = new_connect_button
