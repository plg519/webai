import argparse
import sys
from Classes.VisionCrawler import VisionCrawler


# initial_pause_time = 3
# num_scrolls = 200
# num_mouse_clicks_per_scroll = 5
# pause_time_between_scrolls = 2
# profile_key_words = ["Engineer", "Developer"]
# profile_key_words = ["Manager", "Capital"]
# profile_key_words = ["CEO", "founder", "Analyst", "Associate"]
# wanted_num_invitations = 3

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    try:

        # parser.add_argument("--keywords", type=list, help = "keywords for connect", required=True)
        parser.add_argument('-k','--keywords', type=str, help = "keywords for sending invitations")

        # parser.add_argument("--initial_pause_time", type=float, default=3.0, help = "initial waiting time")
        parser.add_argument('-n', '--num_invitations', type=int, default=1, help = "number of invitations wanted to "
                                                                             "send before program terminates")



        args = parser.parse_args()

        profile_key_words = [str(word.strip()) for word in args.keywords.split(',')]
        wanted_num_invitations = args.num_invitations
        initial_pause_time = 2
        pause_time_between_scrolls = 1.5

        print('Requested_num_invitations: ', wanted_num_invitations)

    except:
        print("Usage: python main.app -k 'keyword1, keyword2, ...', -n 'num_invitations_to_send' ")
        sys.exit(1)

    VisionCrawler.pause(initial_pause_time)
    VisionCrawler.keep_scrolling_and_connecting(pause_time_between_scrolls,
                                                profile_key_words, wanted_num_invitations)

    print("All Done")
    sys.exit(0)