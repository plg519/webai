import pyautogui
from pyautogui import tweens
import time
import cv2
import numpy as np
from Classes.RegionDetector import RegionDetector
from Classes.ColorImage import ColorImage


time.sleep(3)

screenshot = pyautogui.screenshot()
# print("cv_im.shape: ", cv_im.shape)


# print("screenshot: ", screenshot)
# screenshot.show()

cv_im = np.array(screenshot)

print("cv_im.shape: ", cv_im.shape)

# cv_im = cv2.cvtColor(cv_im, cv2.COLOR_BGR2RGB)
cv_im = cv2.cvtColor(cv_im, cv2.COLOR_RGB2BGR)


test_im = ColorImage(pixel_values=cv_im)

# print("test_im", test_im.pixel_values.size)

resized_im = test_im.resize(height=900)
# resized_im = test_im
# print("resized_im", resized_im.pixel_values)

# resized_im.show()

# detector = RegionDetector()


dilation_x=2
dilation_y=3
dilation_num_iterations=5
side_margin_ratio = 0.2

# detected_regions = detector.detect_regions(resized_im, dilation_x, dilation_y, dilation_num_iterations)
# detected_regions = detector.detect_circles(resized_im)
# detected_regions = detector.detect_linkedin_connect(resized_im, dilation_x, dilation_y, dilation_num_iterations, side_margin)

# detected_regions = RegionDetector.detect_profile_photos(resized_im, side_margin)
profile_regions = RegionDetector.detect_profile_photos(resized_im, side_margin_ratio)
# for region in detected_regions:
#     region.show()
#
connect_regions = RegionDetector.detect_linkedin_connect(resized_im, dilation_x, dilation_y, dilation_num_iterations, side_margin_ratio)

resized_im.add_detected_regions(profile_regions)
resized_im.add_detected_regions(connect_regions)

v_thresh = 215
h_thresh = 20

resized_im.merge_photo_connect_to_profile(v_thresh, h_thresh)

top_region_cate = "profile_photo"
bottom_region_cate = "connect_button"
resized_im.add_photo_connect_middle_subregion(top_region_cate, bottom_region_cate)

resized_im.add_positions_connections_subregions()

resized_im.ocr_position_regions()

category = "profile"
# resized_im.draw_detected_regions(category)
resized_im.draw_detected_regions_with_subregions(category)



resized_im.show()
