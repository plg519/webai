from Classes.ColorImage import ColorImage
from Classes.RegionDetector import RegionDetector

class Agent:

    @staticmethod
    def get_profiles(color_image, side_margin_ratio, dilation_x, dilation_y, dilation_num_iterations):
        profile_photo_regions = RegionDetector.detect_profile_photos(color_image, side_margin_ratio)
        connect_button_regions = RegionDetector.detect_linkedin_connect(color_image, dilation_x, dilation_y, dilation_num_iterations, side_margin_ratio)
        #to-do: use x,y coordinates to find mathes of each profile photo and connect button
