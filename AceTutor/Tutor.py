from copy import copy
# from dependencies.copy import copy
from Classes.ImageUnderstander import ImageUnderstander
from re import findall
# from dependencies.re import findall
import re


class Tutor:
    def __init__(self, url, name, profile_photo, location, num_views, description, tutoring_experience, subjects,
                 acceptable_hourly_rate,
                 highlights, educations, summary, rating=-1):
        self._url = url
        self._name = name
        self._profile_photo = copy(profile_photo)
        self._location = location
        self._num_views = num_views
        self._description = description
        self._tutoring_experience = tutoring_experience
        self._subjects = list(subjects)
        # self._subjects = subjects
        self._acceptable_hourly_rate = acceptable_hourly_rate
        self._highlights = highlights
        self._educations = list(educations)
        self._summary = summary
        self._num_missing_fields = -1
        self._tutoring_years = -1
        self._face_present = -1,
        self._age = -1,
        self._gender = -1,
        self._beauty_score = -1,
        self._relative_x1 = 0,
        self._relative_y1 = 0,
        self._relative_h = 0,
        self._relative_w = 0,
        self._num_words_in_description = 0,
        self._num_words_in_summary = 0,
        self._university_ranking = -1,
        self._professional_score = -1,

        self._rating = rating

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, new_name):
        self._name = new_name

    @property
    def profile_photo(self):
        return self._profile_photo

    @profile_photo.setter
    def profile_photo(self, new_profile_photo):
        self._profile_photo = new_profile_photo

    @property
    def location(self):
        return self._location

    @location.setter
    def location(self, new_location):
        self._location = new_location

    @property
    def num_views(self):
        return self._num_views

    @num_views.setter
    def num_views(self, new_num_views):
        self._num_views = new_num_views

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, new_description):
        self._description = new_description

    @property
    def tutoring_experience(self):
        return self._tutoring_experience

    @tutoring_experience.setter
    def tutoring_experience(self, new_tutoring_experience):
        self._tutoring_experience = new_tutoring_experience

    @property
    def subjects(self):
        return self._subjects

    @subjects.setter
    def subjects(self, new_subjects):
        self._subjects = new_subjects

    @property
    def acceptable_hourly_rate(self):
        return self._acceptable_hourly_rate

    @acceptable_hourly_rate.setter
    def acceptable_hourly_rate(self, new_acceptable_hourly_rate):
        self._acceptable_hourly_rate = new_acceptable_hourly_rate

    @property
    def highlights(self):
        return self._highlights

    @highlights.setter
    def highlights(self, new_highlights):
        self._highlights = new_highlights

    @property
    def educations(self):
        return self._educations

    @educations.setter
    def educations(self, new_educations):
        self._educations = new_educations

    @property
    def summary(self):
        return self._summary

    @summary.setter
    def summary(self, new_summary):
        self._summary = new_summary

    @property
    def rating(self):
        return self._rating

    @rating.setter
    def rating(self, new_rating):
        self._rating = new_rating

    @property
    def num_missing_fields(self):
        return self._num_missing_fields

    @num_missing_fields.setter
    def num_missing_fields(self, new_num_missing_fields):
        self._num_missing_fields = new_num_missing_fields

    @property
    def tutoring_years(self):
        return self._tutoring_years

    @tutoring_years.setter
    def tutoring_years(self, new_tutoring_years):
        self._tutoring_years = new_tutoring_years

    @property
    def age(self):
        return self._age

    @age.setter
    def age(self, new_age):
        self._age = new_age

    @property
    def gender(self):
        return self._gender

    @gender.setter
    def gender(self, new_gender):
        self._gender = new_gender

    @property
    def beauty_score(self):
        return self._beauty_score

    @beauty_score.setter
    def beauty_score(self, new_beauty_score):
        self._beauty_score = new_beauty_score

    @property
    def relative_x1(self):
        return self._relative_x1

    @relative_x1.setter
    def relative_x1(self, new_relative_x1):
        self._relative_x1 = new_relative_x1

    @property
    def relative_y1(self):
        return self._relative_y1

    @relative_y1.setter
    def relative_y1(self, new_relative_y1):
        self._relative_y1 = new_relative_y1

    @property
    def relative_w(self):
        return self._relative_w

    @relative_w.setter
    def relative_w(self, new_relative_w):
        self._relative_w = new_relative_w

    @property
    def relative_h(self):
        return self._relative_h

    @relative_h.setter
    def relative_h(self, new_relative_h):
        self._relative_h = new_relative_h

    @property
    def num_words_in_description(self):
        return self._num_words_in_description

    @num_words_in_description.setter
    def num_words_in_description(self, new_num_words_in_description):
        self._num_words_in_description = new_num_words_in_description

    @property
    def num_words_in_summary(self):
        return self._num_words_in_summary

    @num_words_in_summary.setter
    def num_words_in_summary(self, new_num_words_in_summary):
        self._num_words_in_summary = new_num_words_in_summary

    @property
    def university_ranking(self):
        return self._university_ranking

    @university_ranking.setter
    def university_ranking(self, new_university_ranking):
        self._university_ranking = new_university_ranking

    @property
    def face_present(self):
        return self._face_present

    @face_present.setter
    def face_present(self, new_face_present):
        self._face_present = new_face_present

    @property
    def professional_score(self):
        return self._professional_score

    @professional_score.setter
    def professional_score(self, new_professional_score):
        self._professional_score = new_professional_score

    @property
    def url(self):
        return self._url

    @url.setter
    def url(self, new_url):
        self._url = new_url

    def display_into(self):
        print("")
        print("url: ", self._url)
        print("name: ", self._name, "")
        print("location: ", self._location)
        print("num_views: ", self._num_views)
        print("description: ", self._description)
        print("tutoring_experience: ", self._tutoring_experience)
        print("subjects: ", self._subjects)
        print("acceptable_hourly_rate: ", self._acceptable_hourly_rate)
        print("educations: ", self._educations)
        print("summary: ", self._summary, "\n")
        print("num_missing_fields: ", self._num_missing_fields)
        print("tutoring_years: ", self._tutoring_years)
        print("face_present: ", self._face_present)
        print("age: ", self._age)
        print("gender: ", self._gender)
        print("beauty_score: ", self._beauty_score)
        print("professional_score: ", self._professional_score)
        print("face relative x, y, w, h: ", self._relative_x1, self._relative_y1, self._relative_w, self._relative_h)
        print("num_words_in_description: ", self._num_words_in_description)
        print("num_words_in_summary: ", self._num_words_in_summary)
        print("university_ranking: ", self._university_ranking)

        print("\nrating: ", self._rating)
        # self._profile_photo.show()
        # for face_region in self.profile_photo.sub_regions:
        #     face_region.show()
        # cv2.imshow("", self._profile_photo)
        # while (cv2.waitKey() != ord('q')):
        #     continue
        # cv2.destroyAllWindows()

    def compute_set_num_missing_fields(self):
        num_missing_fields = 0
        if self.name == "":
            num_missing_fields += 1
        if "Not specified" in self._tutoring_experience:
            num_missing_fields += 1
        if len(self._subjects) == 0:
            num_missing_fields += 1
        if len(self._educations) == 0:
            num_missing_fields += 1
        if self._summary == "":
            num_missing_fields += 1
        self._num_missing_fields = num_missing_fields

    def set_num_tutoring_years(self):
        if "years" in self._tutoring_experience:
            # print(self._tutoring_experience)
            # strs = [s for s in self._tutoring_experience.split()]
            # print("strs: ", strs)
            numbers = [int(s) for s in self._tutoring_experience.split() if s.isdigit()]
            # print("numbers: ", numbers)
            self._tutoring_years = min(numbers)
        else:
            self._tutoring_years = 0

    def compute_set_visual_semantic_features(self, face_detector, age_gender_model, img_size, beauty_model,
                                             beauty_img_width, beauty_img_height, professionalness_model):
        ImageUnderstander.set_detected_faces(face_detector, self.profile_photo)

        self._face_present = self.has_detected_face()

        self._age, self._gender = ImageUnderstander.compute_age_gender(age_gender_model, self.profile_photo, img_size)

        self._beauty_score = ImageUnderstander.compute_beauty_score(beauty_model, self.profile_photo, beauty_img_width,
                                                                    beauty_img_height)

        self._professional_score = ImageUnderstander.compute_professional_score(professionalness_model,
                                                                                self.profile_photo, beauty_img_width,
                                                                                beauty_img_height)

        self._relative_x1, self._relative_y1, self._relative_w, self._relative_h = ImageUnderstander.compute_face_position(
            self.profile_photo)

    def compute_set_num_words_in_description(self):
        words = [s for s in self._description.split()]
        self._num_words_in_description = len(words)

    def compute_set_num_words_in_summary(self):
        words = [s for s in self._summary.split()]
        self._num_words_in_summary = len(words)

    def compute_set_university_ranking(self, university_ranking_d):
        universities = set()
        rankings = set()
        for education in self._educations:
            if "University" in education or "College" in education or "Institute" in education or "School" in education:
                universities.add(education)
            # print("universities: ", universities)
        for university in universities:
            # print("university: ", university)
            ranking_str = str(university_ranking_d[university])
            # print("ranking_str: ", ranking_str)
            numbers = findall(r"\d+", ranking_str)
            # print("numbers: ", numbers)
            ranking = int(min(numbers))
            # print("ranking: ", ranking)
            if ranking > 0:
                rankings.add(ranking)
        # print("rankings: ", rankings)
        if len(rankings) > 0:
            self._university_ranking = min(rankings)
        else:
            self._university_ranking = 500

    def to_dict(self):
        return {"num_missing_fields": self._num_missing_fields,
                "tutoring_years": self._tutoring_years,
                "face_present": self._face_present,
                "age": self._age,
                "gender": self._gender,
                "beauty_score": self._beauty_score,
                "professional_score": self._professional_score,
                "relative_x": self._relative_x1,
                "relative_y": self._relative_y1,
                "relative_w": self._relative_w,
                "relative_h": self._relative_h,
                "num_words_in_description": self._num_words_in_description,
                "num_words_in_summary": self._num_words_in_summary,
                "university_ranking": self._university_ranking,
                "rating": self._rating}

    def has_detected_face(self):
        for region in self.profile_photo.sub_regions:
            if region.region_type == "face_region":
                return True
        return False

    def get_tutor_id_by_url(self):
        return self._url.split('/')[-1]

    def to_dict_firestore(self):
        profile_photo_object = {
            u'bytes_of_photo': self._profile_photo.pixel_values.tobytes(),
            u'data_type': self._profile_photo.pixel_values.dtype.name,
            u'array_shape': list(self._profile_photo.pixel_values.shape)
        }

        return {
            u'url': self._url,
            u'name': self._name,
            u'profile_photo': profile_photo_object,
            u'location': self._location,
            u'num_views': self._num_views,
            u'description': self._description,
            u'tutoring_experience': self._tutoring_experience,
            u'subjects': self._subjects,
            u'acceptable_hourly_rate': self._acceptable_hourly_rate,
            u'highlights': self._highlights,
            u'educations': self._educations,
            u'summary': self._summary,
            u'num_missing_fields': self._num_missing_fields,
            u'tutoring_years': self._tutoring_years,
            u'face_present': self._face_present,
            u'age': self._age,
            u'gender': self._gender,
            u'beauty_score': self._beauty_score,
            u'relative_x1': self._relative_x1,
            u'relative_y1': self._relative_y1,
            u'relative_h': self._relative_h,
            u'relative_w': self._relative_w,
            u'num_words_in_description': self._num_words_in_description,
            u'num_words_in_summary': self._num_words_in_summary,
            u'university_ranking': self._university_ranking,
            u'professional_score': self._professional_score,
            u'rating': self._rating
        }
