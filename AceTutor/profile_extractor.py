from Classes.LayoutDetector import LayoutDetector
from Classes.HtmlCrawler import save_screenshots
import cv2
from Classes.ColorImage import ColorImage
from os import listdir
from os.path import isfile, join

def get_common_field_names(profile_sceenshots_folder_path):
    screeshot_paths = [f for f in listdir(profile_sceenshots_folder_path) if isfile(join(profile_sceenshots_folder_path, f))]

    ims = []
    for screenshot_name in screeshot_paths:
        screenshot_path = join(profile_sceenshots_folder_path, screenshot_name)
        print("screenshot_path: ", screenshot_path)
        profile_cv_im = cv2.imread(screenshot_path)
        profile_im = ColorImage(pixel_values=profile_cv_im)
        ims.append(profile_im)

    common_field_names= LayoutDetector.find_common_field_names(ims)
    # print("common_field_names: ", common_field_names)
    return common_field_names

def find_common_fields_names(profile_sceenshots_folder_path):
    screeshot_paths = [f for f in listdir(profile_sceenshots_folder_path) if isfile(join(profile_sceenshots_folder_path, f)) and f.split(".")[-1] == "png"]

    ims = []
    for screenshot_name in screeshot_paths:
        screenshot_path = join(profile_sceenshots_folder_path, screenshot_name)
        print("screenshot_path: ", screenshot_path)
        profile_cv_im = cv2.imread(screenshot_path)
        profile_im = ColorImage(pixel_values=profile_cv_im)
        ims.append(profile_im)
        # profile_im.show()

    common_names = LayoutDetector.find_common_field_names(ims)
    return common_names, ims

def create_tutors_by_field_names(profile_sceenshots_folder_path, additional_names):
    tutors = []
    common_field_names, ims = find_common_fields_names(profile_sceenshots_folder_path)
    common_field_names.add("Summary")
    print("common_field_names: ", common_field_names)
    for name in additional_names:
        common_field_names.add(name)
    LayoutDetector.add_common_field_regions(ims, common_field_names)
    for im in ims:
        im.sort_regions_by_top_down_order()
        # im.draw_all_detected_regions()
        # im.show()
        tutors.append(im.create_tutor())
    return tutors


# profile_urls = [r"https://www.acetutor.io/prahladmenon",
#                 r"https://www.acetutor.io/alinenaroditsky",
#                 r"https://www.acetutor.io/sophiatsai"]

profile_urls = [r"https://www.acetutor.io/ace/prahladmenon",
                r"https://www.acetutor.io/ace/alinenaroditsky",
                r"https://www.acetutor.io/ace/sophiatsai"]

profile_sceenshots_folder_path = r"/Users/plg519/WebAI_data/profile_screenshots"

# save_screenshots(profile_urls, profile_sceenshots_folder_path, pause_duration = 10.0)


add_field_names = ["Summary"]
tutors = create_tutors_by_field_names(profile_sceenshots_folder_path, add_field_names)
for tutor in tutors:
    tutor.display_into()


