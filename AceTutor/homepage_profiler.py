import argparse
import sys
from Classes.VisionCrawler import VisionCrawler


initial_pause_time = 3
# num_scrolls = 200
# num_mouse_clicks_per_scroll = 5
pause_time_between_scrolls = 2
# profile_key_words = ["Engineer", "Developer"]
# profile_key_words = ["Manager", "Capital"]
# profile_key_words = ["CEO", "founder", "Analyst", "Associate"]
# wanted_num_invitations = 3

if __name__ == '__main__':
    VisionCrawler.pause(initial_pause_time)
    VisionCrawler.keep_scrolling_and_detecting(pause_time_between_scrolls)

