from AceTutor.Preprocessor import Preprocessor
# import AceTutor.Preprocessor as Preprocessor_Module
from Classes.ImageUnderstander import ImageUnderstander
from Classes.Trainer import Trainer

csv_path = r"/Users/plg519/WebAI_data/Ace/profile_url_ratings.csv"
profile_screenshots_dir = r"/Users/plg519/WebAI_data/Ace/profile_screenshots/"
save_screenshots_flag = False
common_field_names = {'Tutoring Experience', 'Location', 'Educations', 'Proﬁle', 'Privacy Policy', '0 Review', 'Terms of Use', 'luau U" ”V?\nApp Store', '@All rights reserved - Ace Tutor 2018', 'Summary', 'GIYIION\n\n/ Google Play', 'Acceptable Hourly Rate', 'Copy Link', 'Share', 'Highlights', 'Subjects'}

# test_num_tutors = 5
# test_num_tutors = 10
# test_num_tutors = 20
# test_num_tutors = 40

age_gender_pre_trained_model_path = r"/Users/plg519/WebAI/ContentUnderstander/age-gender-estimation/pretrained_models/weights.28-3.73.hdf5"
beauty_trained_model_path = r"/Users/plg519/WebAI/ContentUnderstander/face-attractness-prediction/Facial-Beauty-Prediction/weights.best.from_scratch_all_layers.hdf5"
profesionalness_trained_model_path = r"/Users/plg519/WebAI_data/Ace/Professionalness/model/weights.best.from_scratch_all_layers.hdf5"
university_ranking_json_path = r"/Users/plg519/WebAI_data/world_university_ranking_2018.json"
face_detector_model_path = r"/Users/plg519/WebAI/ContentUnderstander/face_detection_pretrained_model/haarcascade_frontalface_default.xml"


df_save_csv_path = r"/Users/plg519/WebAI_data/Ace/tutors_df_w_prof.csv"
save_figure_path = r"/Users/plg519/WebAI_data/Ace/pred_true_w_prof.png"
true_pred_csv_path = r"/Users/plg519/WebAI_data/Ace/true_predictions_w_prof.csv"

save_tutors_path = r"/Users/plg519/WebAI_data/Ace/all_tutors_w_prof_score.pickle"
save_model_path = r"/Users/plg519/WebAI_data/Ace/tutor_rating_model_w_prof.pickle"

url_photos_path = r"/Users/plg519/WebAI/data_and_models/url_photos.txt"

create_tutors_from_scratch_flag = True

if create_tutors_from_scratch_flag:
    url_path_rating_d = Preprocessor.save_screenshots_get_url_path_rating_d(csv_path, profile_screenshots_dir, save_screenshots_flag)
    url_path_rating_d = {k: url_path_rating_d[k] for k in list(url_path_rating_d)[:10]}
    # url_path_rating_d = {k: url_path_rating_d[k] for k in list(url_path_rating_d)}

    print("url_path_rating_d: ", url_path_rating_d)

    tutors = Preprocessor.create_tutors_by_field_names(url_path_rating_d, common_field_names)

    Preprocessor.set_profile_photo(tutors, url_photos_path)

    Preprocessor.compute_set_meta_features(tutors)

    image_understander = ImageUnderstander()
    Preprocessor.compute_set_visual_semantic_features(tutors, image_understander,
                                                      age_gender_pre_trained_model_path, beauty_trained_model_path,
                                                      profesionalness_trained_model_path, face_detector_model_path)

    Preprocessor.compute_set_language_features(tutors, university_ranking_json_path)

    Preprocessor.save_objects_to_pickles(tutors, save_tutors_path)

else:
    tutors = Preprocessor.load_objs_from_pickle(save_tutors_path)

# for tutor in tutors:
#     tutor.display_into()

Trainer.convert_to_df_write_to_csv(tutors, df_save_csv_path)

train_tutors, test_tutors = Trainer.split_data(tutors, test_raio=0.1, set_seed=0)
# for tutor in test_tutors:
#     tutor.display_into()

#data imputation
# train_tutors = Preprocessor.impute_train_tutors(train_tutors)
test_tutors = Preprocessor.impute_test_tutors(train_tutors, test_tutors)

print("num train tutors: ", len(train_tutors))
print("num test tutors: ", len(test_tutors))

# for train_tutor in train_tutors:
#     print("train_tutor.age: ", train_tutor.age)


model = Trainer.train(train_tutors, save_model_path)

test_tutor_ratings = Trainer.get_labels(test_tutors)
predicted_test_tutor_ratings = Trainer.predict(test_tutors, model)
# print("predicted_test_tutor_ratings: ", predicted_test_tutor_ratings)

rmse = Trainer.compute_RMSE(test_tutor_ratings, predicted_test_tutor_ratings)
print("rmse: ", rmse)

Trainer.save_pred_true_to_csv(test_tutor_ratings, predicted_test_tutor_ratings, true_pred_csv_path)
# Trainer.save_prediction_true_plot(test_tutor_ratings, predicted_test_tutor_ratings, save_figure_path, rmse)
