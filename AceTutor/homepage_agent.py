import pyautogui
from pyautogui import tweens
import time
import cv2
import numpy as np
from pytesseract import image_to_string

def image_resize(image, width = None, height = None, inter = cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv2.resize(image, dim, interpolation = inter)

    # return the resized image
    return resized

def segment_words_characters(imgInput, MIN_CONTOUR_AREA = 0):
    # imgInput = cv2.imread("nYOYA.png")

    imgInput = cv2.resize(imgInput, (imgInput.shape[1] * 2, imgInput.shape[0] * 2))

    cv2.imshow("input:", imgInput)


    # convert image to grayscale
    imgGray = cv2.cvtColor(imgInput, cv2.COLOR_BGR2GRAY)

    # invert black and white
    newRet, binaryThreshold = cv2.threshold(imgGray, 127, 255, cv2.THRESH_BINARY_INV)

    # dilation
    # rectkernel = cv2.getStructuringElement(cv2.MORPH_RECT, (15, 10))
    rectkernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 3))


    # rectdilation = cv2.dilate(binaryThreshold, rectkernel, iterations=1)
    rectdilation = cv2.dilate(binaryThreshold, rectkernel, iterations=9)


    outputImage = imgInput.copy()

    # _, npaContours, npaHierarchy = cv2.findContours(rectdilation.copy(),
    #                                              cv2.RETR_EXTERNAL,
    #                                              cv2.CHAIN_APPROX_SIMPLE)



    _, npaContours, npaHierarchy = cv2.findContours(rectdilation.copy(),
                                                 cv2.RETR_EXTERNAL,
                                                 cv2.CHAIN_APPROX_NONE)



    print("len(npaContours): ", len(npaContours))

    for npaContour in npaContours:
        if cv2.contourArea(npaContour) > MIN_CONTOUR_AREA:

            [intX, intY, intW, intH] = cv2.boundingRect(npaContour)

            print([intX, intY, intW, intH])

            cv2.rectangle(outputImage,
                          (intX, intY),  # upper left corner
                          (intX + intW, intY + intH),  # lower right corner
                          (0, 0, 255),  # red
                          2)  # thickness

            # Get subimage of word and find contours of that word
            imgROI = binaryThreshold[intY:intY + intH, intX:intX + intW]

            cv2.imshow("imgROI: ", imgROI)
            cv2.waitKey(0)

            _, subContours, subHierarchy = cv2.findContours(imgROI.copy(),
                                                         cv2.RETR_EXTERNAL,
                                                         cv2.CHAIN_APPROX_SIMPLE)

            print("len(subContours): ", len(subContours))

            # This part is not working as I am expecting
            for subContour in subContours:
                [pointX, pointY, width, height] = cv2.boundingRect(subContour)

                # cv2.rectangle(outputImage, (intX + pointX, intY + pointY), (intX + width, intY + height), (0, 255, 0), 2)
                cv2.rectangle(outputImage, (intX + pointX, intY + pointY), (intX + pointX + width, intY + pointY + height), (0, 255, 0), 2)

    cv2.imshow("original", imgInput)
    cv2.imshow("rectdilation", rectdilation)
    cv2.imshow("threshold", binaryThreshold)
    cv2.imshow("outputRect", outputImage)

    cv2.waitKey(0);

def segment_characters(im):
    im = cv2.resize(im, (im.shape[1]*2, im.shape[0]*2))
    mser = cv2.MSER_create()
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    vis = im.copy()
    regions = mser.detectRegions(gray)
    # hulls = [cv2.convexHull(p.reshape(-1, 1, 2)) for p in regions[0]]
    hulls = cv2.convexHull(gray.reshape(-1, 1, 2))

    cv2.polylines(vis, hulls, 1, (0, 255, 0))

    cv2.namedWindow("im", 0)
    cv2.imshow("im", vis)
    while(cv2.waitKey()!=ord('q')):
        continue
    cv2.destroyAllWindows()

def captch_ex(img, dilation_x, dilation_y, dilation_num_iterations):
    # img = cv2.imread(file_name)
    # img_final = cv2.imread(file_name)

    img_final = img.copy()

    img2gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, mask = cv2.threshold(img2gray, 180, 255, cv2.THRESH_BINARY)
    image_final = cv2.bitwise_and(img2gray, img2gray, mask=mask)
    ret, new_img = cv2.threshold(image_final, 180, 255, cv2.THRESH_BINARY_INV)  # for black text , cv.THRESH_BINARY_INV
    '''
            line  8 to 12  : Remove noisy portion 
    '''
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (dilation_x,
                                                         dilation_y))  # to manipulate the orientation of dilution , large x means horizonatally dilating  more, large y means vertically dilating more
    dilated = cv2.dilate(new_img, kernel, iterations=dilation_num_iterations)  # dilate , more the iteration more the dilation

    # for cv2.x.x

    _, contours, hierarchy = cv2.findContours(dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)  # findContours returns 3 variables for getting contours

    # for cv3.x.x comment above line and uncomment line below

    #image, contours, hierarchy = cv2.findContours(dilated,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)


    for contour in contours:
        # get rectangle bounding contour
        [x, y, w, h] = cv2.boundingRect(contour)

        # Don't plot small false positives that aren't text
        if w < 35 and h < 35:
            continue

        # draw rectangle around contour on original image
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 255), 2)

        '''
        #you can crop image and send to OCR  , false detected will return no text :)
        cropped = img_final[y :y +  h , x : x + w]

        s = file_name + '/crop_' + str(index) + '.jpg' 
        cv2.imwrite(s , cropped)
        index = index + 1

        '''

        cropped = img_final[y:y + h, x: x + w]
        # segment_words_characters(cropped)

        text = image_to_string(cv2.resize(cropped, (cropped.shape[1]*2, cropped.shape[0]*2)), lang="eng")
        print("text: ", text)

        #to-do:
        # could try to add a little to character
        # develop clustering algo. based on average character size (or width, height, color histogram) to separate different fonts
        # based on different fonts, know title, content, others
        # use mouse auto select and copy instead of OCR for text extraction
        # Tune paras for ace homepage, then find each sub-links, based on common words (educations, etc.) to find the structural fileds


    # write original image with added contours to disk
    cv2.imshow('captcha_result', img)
    while(cv2.waitKey()!=ord('q')):
        continue
    cv2.destroyAllWindows()


# file_name = 'your_image.jpg'
# captch_ex(file_name)

time.sleep(3)
screenWidth, screenHeight = pyautogui.size()
currentMouseX, currentMouseY = pyautogui.position()

# pyautogui.moveTo(1000, 433)
# pyautogui.click()
# time.sleep(2)

screenshot = pyautogui.screenshot()

# print("screenshot: ", screenshot)
# screenshot.show()

cv_im = np.array(screenshot)
resized_cv_im = image_resize(cv_im, height=800)

dilation_x = 2
dilation_y = 3
dilation_num_iterations = 9

captch_ex(resized_cv_im, dilation_x, dilation_x, dilation_num_iterations)

time.sleep(5)

for i in range(5):
    pyautogui.scroll(-10, pause=1.)