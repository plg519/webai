import datetime


class TutorAttributeFirestore:
    def __init__(self, attribute):
        self._data = list(attribute)
        self._date = datetime.date.today().strftime("%Y-%m-%d")

    @property
    def data(self):
        return self._data

    @property
    def date(self):
        return self._date

    def to_dict_firestore(self):
        return {
            u'data': self._data,
            u'date': self._date
        }
