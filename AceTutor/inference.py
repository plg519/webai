import sys

sys.path.append('/Users/plg519/WebAI')

from selenium import webdriver
from PIL import Image
import io
import numpy as np
# import matplotlib.pyplot as plt
from Classes.LayoutDetector import LayoutDetector
from AceTutor.Preprocessor import Preprocessor
from Classes.ImageUnderstander import ImageUnderstander
from Classes.Trainer import Trainer
from Classes.ColorImage import ColorImage
from os.path import join
from Classes.FirestoreHandler import FirestoreHandler
import pickle
import os


def get_profile_ims_by_urls(urls):
    driver = webdriver.PhantomJS(resource_path(PHANTOMJS_EXECUTABLE_PATH))

    profile_urls_ims = []

    for url in urls:
        driver.get(url)
        # im_data = driver.get_screenshot_as_png()
        im_data = driver.get_screenshot_as_png()
        image = Image.open(io.BytesIO(im_data))
        # plt.imshow(np.asarray(image))
        # plt.show()
        # print("type(image): ", type(image))
        # cv_im = np.array(image)[..., :3]
        cv_im = np.asarray(image)[..., :3]

        # print(multiarray.array(image))
        # cv_im = multiarray.array(image)[..., :3]
        # cv_im = asarray(image, dtype=np.uint8)[..., :3]
        # a, b, c = cv_im.shape
        # cv_im = array.array('B', cv_im.reshape(a*b*c, 1))
        # cv_im = np.array(cv_im, shape=(a, b, c))
        # cv_im = np.asarray(image, dtype=np.uint8)[..., :3]

        # print(cv_im)
        # print("cv_im.shape: ", cv_im.shape)
        # print("type(cv_im): ", type(cv_im))
        im_array = cv_im[..., ::-1]
        profile_im = ColorImage(pixel_values=im_array)
        profile_urls_ims.append((url, profile_im))

    return profile_urls_ims


def create_tutor(profile_im, common_field_names):
    LayoutDetector.add_detected_regions(profile_im)
    profile_im.add_common_field_regions(common_field_names)
    profile_im.sort_regions_by_top_down_order()

    # for region in profile_im._detected_regions:
    #     region.show()

    if len(profile_im._detected_regions) < 10:
        print("detected region less than 10, page display not analyzable")
        return None

    tutor = profile_im.create_tutor()
    return tutor


def create_tutor_v3(profile_url, profile_im, common_field_names):
    LayoutDetector.add_detected_regions(profile_im)
    profile_im.add_common_field_regions(common_field_names)
    profile_im.sort_regions_by_top_down_order()

    # for region in profile_im._detected_regions:
    #     region.show()

    if len(profile_im._detected_regions) < 10:
        print("detected region less than 10, page display not analyzable")
        return None

    tutor = profile_im.create_tutor_v3(profile_url)
    return tutor


def predict_meta_rating(tutor_url):
    urls = []
    if isinstance(tutor_url, str):
        urls.append(tutor_url)
    if isinstance(tutor_url, list):
        urls.extend(tutor_url)

    with open(resource_path(model_path), 'rb') as file:
        model = pickle.load(file)

    profile_urls_ims = get_profile_ims_by_urls(urls)
    tutors = []

    image_understander = ImageUnderstander()

    for profile_url, profile_im in profile_urls_ims:
        try:
            tutor = create_tutor_v3(profile_url, profile_im, common_field_names)
        except:
            continue
        if tutor is not None:
            tutors.append(tutor)

    Preprocessor.compute_set_meta_features(tutors)

    Preprocessor.compute_set_visual_semantic_features(tutors, image_understander,
                                                      resource_path(age_gender_pre_trained_model_path),
                                                      resource_path(beauty_trained_model_path),
                                                      resource_path(profesionalness_trained_model_path),
                                                      resource_path(face_detector_model_path))

    Preprocessor.compute_set_language_features(tutors, resource_path(university_ranking_json_path))

    predicted_ratings = Trainer.predict(tutors, model)

    assert (len(predicted_ratings == len(tutors)))

    for i in range(len(tutors)):
        tutors[i].rating = predicted_ratings[i]

    return tutors


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS,
        # and places our data files in a folder relative to that temp
        # folder named as specified in the datas tuple in the spec file
        base_path = join(sys._MEIPASS, 'data')
    except Exception:
        # sys._MEIPASS is not defined, so use the original path
        base_path = basepath

    return join(base_path, relative_path)


def save_list_to_pickle(alist, save_path):
    with open(save_path, 'wb') as f:
        pickle.dump(alist, f)


def load_list_from_pickle(save_path):
    with open(save_path, 'rb') as f:
        data = pickle.load(f)
    return data


basepath = r"/Users/plg519/WebAI/data_and_models"
# basepath = r"/Users/haokang/Documents/webai/data_and_models"

PHANTOMJS_EXECUTABLE_PATH = "phantomjs"
age_gender_pre_trained_model_path = "age_gender_pre_trained_weights.28-3.73.hdf5"
beauty_trained_model_path = "beauty_pre_trained_weights.best.from_scratch_all_layers.hdf5"
profesionalness_trained_model_path = "professionalness_pre_traind_weights.best.from_scratch_all_layers.hdf5"
university_ranking_json_path = "world_university_ranking_2018.json"
face_detector_model_path = "haarcascade_frontalface_default.xml"
model_path = "tutor_rating_model_w_prof.pickle"
common_field_names = {'Tutoring Experience', 'Location', 'Educations', 'Proﬁle', 'Privacy Policy', '0 Review',
                      'Terms of Use', 'luau U" ”V?\nApp Store', '@All rights reserved - Ace Tutor 2018', 'Summary',
                      'GIYIION\n\n/ Google Play', 'Acceptable Hourly Rate', 'Copy Link', 'Share', 'Highlights',
                      'Subjects'}

# tutor_urls = [r"https://www.acetutor.io/ace/sagarpatel",
#               r"https://www.acetutor.io/ace/meganeriksen",
#               r"https://www.acetutor.io/ace/eria6",
#               r"https://www.acetutor.io/ace/tammymontero"]

# tutor_urls = ["https://www.acetutor.io/ace/marcosbenitez",
#               r"https://www.acetutor.io/ace/jodybryant",
#               r"https://www.acetutor.io/ace/kriscole",
#               r"https://www.acetutor.io/ace/robertpowers"]

# tutor_urls = ["https://www.acetutor.io/ace/loridixon",
#               "https://www.acetutor.io/ace/susanibanez",
#               "https://www.acetutor.io/ace/doctorpark"]


# ---------- start comment out this piece of code temporarily ----------
# tutor_urls = "https://www.acetutor.io/ace/doctorpark"

# tutors = predict_meta_rating(tutor_urls)
#
# for tutor in tutors:
#     tutor.display_into()
#
#
# # save_path = r"/Users/plg519/WebAI/data_and_models/test_predicted_tutors.pickle"
# save_path = r"/Users/haokang/Documents/webai/data_and_models/z_test_predicted_tutors.pickle"
# save_list_to_pickle(tutors, save_path)
#
# time.sleep(5)
# ---------- end ----------

# save_path = r"/Users/haokang/Documents/webai/data_and_models/predicted_tutors.pickle"
# predicted_tutors = load_list_from_pickle(save_path)
# cred_path = "/Users/haokang/Documents/webai/data_and_models/tutorsdb-f22d1-7d01c499c3af.json"

cred_path = basepath + "/tutorsdb-f22d1-7d01c499c3af.json"

firestore_handler = FirestoreHandler(cred_path)
# print("predicted tutors from pickle: ")

# for tutor in tutors:
#     tutor.display_into()
#     firestore_handler.save_document(u'tutors', tutor, tutor.get_tutor_id_by_url())

# query by tutor id
# firestore_handler.get_document(u'tutors', u'tiffanyroldan')

# # query by num_views
# firestore_handler.get_multi_documents(u'tutors', u'num_views', '>', 350)
#
# # query by subjects
# firestore_handler.get_multi_documents(u'tutors', u'subjects', u'array_contains', u'English')
# firestore_handler.get_multi_documents(u'tutors', u'subjects', u'array_contains', u'Science')

#
# # query by gender
# firestore_handler.get_multi_documents(u'tutors', u'gender', '==', 0)
#
# # query by beauty_score
# firestore_handler.get_multi_documents(u'tutors', u'beauty_score', '>', 5)
#
# # query by educations
# firestore_handler.get_multi_documents(u'tutors', u'educations', u'array_contains', 'PhD')
#
# # query by rating
# firestore_handler.get_multi_documents(u'tutors', u'rating', '>', 12)
#
# # query by professional_score
# firestore_handler.get_multi_documents(u'tutors', u'professional_score', '>', 0.99999999999999994)

# subjects = set()
# docs_dicts = firestore_handler.get_multi_documents_as_dicts(u'tutors', u'num_views', '>', 350)
# print("len(docs_dict): ", len(docs_dicts))
# for doc_dict in docs_dicts:
#     print(doc_dict['subjects'])

# all_subjects = firestore_handler.get_all_subjects(u'tutors')
# print('all_subjects has ', len(all_subjects), 'subjects \n', all_subjects)
# lower_case_subjects = [s.lower() for s in all_subjects]

# refresh subjects
# firestore_handler.refresh_attribute_collection(u'tutors', u'subjects', u'tutor_attributes')
# all_sub_docs = firestore_handler.get_document(u'tutor_attributes', u'tutors_subjects')
# print("all_sub_docs: \n", all_sub_docs[0]['data'])


# refresh location
# firestore_handler.refresh_attribute_collection(u'tutors', u'location', u'tutor_attributes')
# firestore_handler.get_document(u'tutor_attributes', u'tutors_location')
# firestore_handler.delete_document(u'tutor_attributes', u'tutors_location')

# refresh educations
# firestore_handler.refresh_attribute_collection(u'tutors', u'educations', u'tutor_attributes')
# firestore_handler.get_document(u'tutor_attributes', u'tutors_educations')

# refresh tutoring experience
# firestore_handler.refresh_attribute_collection(u'tutors', u'tutoring_experience', u'tutor_attributes')
# firestore_handler.get_document(u'tutor_attributes', u'tutors_tutoring_experience')

# refresh acceptable hourly rate
# firestore_handler.refresh_attribute_collection(u'tutors', u'acceptable_hourly_rate', u'tutor_attributes')
# firestore_handler.get_document(u'tutor_attributes', u'tutors_acceptable_hourly_rate')

# docs_list = firestore_handler.get_document(u'tutor_attributes', u'tutors_tutoring_experience')
# docs_list = firestore_handler.get_document(u'tutors_w_locations', u'tiffanyroldan')

# categorical_matching_ids = firestore_handler.get_multi_documents_ids(u'tutors_w_locations', u'subjects',
#                                                                      u'array_contains', 'math')
#return ['alex62', 'amelielei', 'cavandonohoe', 'chloe5', 'miriamfarag', 'othniel5', 'samlycooper', 'sophieb', 'vivek9']

# categorical_matching_ids = firestore_handler.get_multi_documents_ids(u'tutors', u'subjects',
#                                                                      u'array_contains', 'math')
#return ['alex62', 'amelielei', 'ashlynngunther', 'blakemcbride', 'cavandonohoe', 'chloe5', 'codiev', 'jacob3', 'laurenhinkley', 'miriamfarag', 'othniel5', 'pa4', 'sagarpatel', 'sophieb', 'tammymontero', 'vivek9']]

# categorical_matching_ids = firestore_handler.get_multi_documents_ids(u'tutors_w_locations', u'locations',
#                                                                      u'array_contains', u'new york, ny')

# categorical_matching_ids = firestore_handler.get_multi_documents_ids(u'tutors_w_locations', u'locations',
#                                                                      u'array_contains', '')

# print(categorical_matching_ids)


# docs_list = firestore_handler.get_document(u'tutor_attributes', u'tutors_locations')
docs_list = firestore_handler.get_document(u'tutors_w_locations', u'miriamfarag')
#return [{'tutoring_years': 3, 'face_present': False, 'num_missing_fields': 3, 'location': ' los angeles, ca',

docs_list = firestore_handler.get_document(u'tutors_w_locations', u'jessemedina')




# categorical_matching_ids = firestore_handler.get_multi_documents_ids(u'tutors_w_locations', u'location',
#                                                                      u'array_contains', ' los angeles, ca')
categorical_matching_ids = firestore_handler.get_multi_documents_ids(u'tutors_w_locations', u'location',
                                                                     u'==', 'santa clara, ca')

# print(docs_list)
print(categorical_matching_ids)


