from Classes.HtmlCrawler import save_screenshots
from collections import defaultdict
import csv
from os.path import join
from Classes.HtmlCrawler import save_screenshot
from Classes.ColorImage import ColorImage
# import Classes.ColorImage as ColorImage_Module

from Classes.RegionDetector import RegionDetector
from Classes.LayoutDetector import LayoutDetector
import cv2
from itertools import islice

from Classes.wide_resnet import WideResNet
from keras.models import Sequential, Model
from keras import applications
from keras.layers import Activation, Dropout, Flatten, Dense,BatchNormalization
from Classes.LanguageUnderstander import LanguageUnderstander

import pickle
import os
import pandas as pd



class Preprocessor:

    @staticmethod
    def save_screenshots_get_url_path_rating_d(csv_path, out_folder, save_screenshots_flag):
        url_path_rating_d = defaultdict(tuple)
        csv_file = open(csv_path, "r")
        read = csv.reader(csv_file, delimiter=" ")
        for row in read:
            url, rating = "https://" + row[0], int(row[1])
            url_last_part = url.split("/")[-1]
            file_name = url_last_part + ".png"
            im_save_path = join(out_folder, file_name)
            url_path_rating_d[url] = (im_save_path, rating)
            if save_screenshots_flag:
                save_screenshot(url, save_path=im_save_path)
                print("url: ", url, " saved to: ", im_save_path)
        return url_path_rating_d

    @staticmethod
    def create_tutors_by_field_names(url_path_rating_d, common_field_names):
        tutors = []

        for url, path_rating in url_path_rating_d.items():
            try:
                screenshot_path, rating = path_rating[0], path_rating[1]
                profile_cv_im = cv2.imread(screenshot_path)
                profile_im = ColorImage(pixel_values=profile_cv_im)

                LayoutDetector.add_detected_regions(profile_im)

                profile_im.add_common_field_regions(common_field_names)

                profile_im.sort_regions_by_top_down_order()
                # profile_im.draw_all_detected_regions()
                # profile_im.show()

                # print("len(profile_im._detected_regions): ", len(profile_im._detected_regions))
                if len(profile_im._detected_regions) < 10:
                    continue

                tutor = profile_im.create_tutor()
                tutor.rating = rating
                tutors.append(tutor)
            except:
                continue

        return tutors

    @staticmethod
    def take_n_elements(n, iterable):
        "Return first n items of the iterable as a list"
        return list(islice(iterable, n))

    @staticmethod
    def compute_set_meta_features(tutors):
        for tutor in tutors:
            tutor.compute_set_num_missing_fields()
            tutor.set_num_tutoring_years()
            # tutor.display_into()

    @staticmethod
    def set_profile_photo(tutors, url_photos_path):
        df = pd.read_csv(url_photos_path, header=None, names=["profile_url", "name", "photo_url"])
        print("df: ", df)
        for tutor in tutors:
            tutor.profile_photo = tutor.set_profile_photo_by_url(df.loc[df["profile_url"] ]["photo_url"])


    @staticmethod
    def compute_set_visual_semantic_features(tutors, image_understander, age_gender_pre_trained_model_path,
                                             beauty_trained_model_path, profesionalness_trained_model_path,
                                             face_detector_model_path,
                                             age_gender_img_size = 64, age_gender_img_width=8, age_gender_img_depth = 16,
                                             beauty_img_width=350, beauty_img_height =350):

        image_understander.init_face_detector(face_detector_model_path)
        face_detector = image_understander.face_detector

        age_gender_model = WideResNet(age_gender_img_size, depth=age_gender_img_depth, k=age_gender_img_width)()
        age_gender_model.load_weights(age_gender_pre_trained_model_path)

        face_resnet = applications.resnet50.ResNet50(include_top=False, weights='imagenet',
                                                input_shape=(beauty_img_width, beauty_img_height, 3), pooling='avg')
        beauty_model = Sequential()
        beauty_model.add(face_resnet)
        beauty_model.add(Dense(1))
        beauty_model.layers[0].trainable = False
        beauty_model.load_weights(beauty_trained_model_path)

        professional_resnet = applications.resnet50.ResNet50(include_top=False, weights='imagenet',
                                                input_shape=(beauty_img_width, beauty_img_height, 3), pooling='max')
        # professional_resnet = applications.resnet50.ResNet50(include_top=True,
        #                                         input_shape=(beauty_img_width, beauty_img_height, 3), pooling='max')

        profesionalness_model = Sequential()
        profesionalness_model.add(professional_resnet)
        profesionalness_model.add(Dense(2))
        profesionalness_model.add(Activation('softmax'))

        profesionalness_model.load_weights(profesionalness_trained_model_path)

        for tutor in tutors:
            # tutor.compute_set_visual_semantic_features(face_detector, age_gender_model, age_gender_img_size, beauty_model, beauty_img_width, beauty_img_height)
            tutor.compute_set_visual_semantic_features(face_detector, age_gender_model, age_gender_img_size, beauty_model, beauty_img_width, beauty_img_height, profesionalness_model)



    @staticmethod
    def compute_set_language_features(tutors, university_ranking_json_path):

        university_ranking_d = LanguageUnderstander.get_university_ranking_d(university_ranking_json_path)

        for tutor in tutors:
            tutor.compute_set_num_words_in_description()
            tutor.compute_set_num_words_in_summary()
            tutor.compute_set_university_ranking(university_ranking_d)

    @staticmethod
    def save_objects_to_pickles(objs, save_path):
        if os.path.isfile(save_path):
            print(save_path, " exists, will not override")
        else:
            print("saving objects to ", save_path)
            with open(save_path, "wb") as handle:
                pickle.dump(objs, handle, protocol=pickle.HIGHEST_PROTOCOL)

    @staticmethod
    def load_objs_from_pickle(saved_path):
        with open(saved_path, "rb") as handle:
            objs = pickle.load(handle)
        return objs




    @staticmethod
    def split_by_face(tutors):
        tutors_with_face = []
        tutors_with_no_face = []
        for tutor in tutors:
            if tutor.has_detected_face():
                tutors_with_face.append(tutor)
            else:
                tutors_with_no_face.append(tutor)
        print("len(tutors_with_face): ", len(tutors_with_face), " len(tutors_with_no_face): ", len(tutors_with_no_face))
        return tutors_with_face, tutors_with_no_face

    @staticmethod
    def get_median_features(tutors):
        features = ["face_present", "age", "beauty_score", "professional_score", "gender", "relative_x", "relative_y", "relative_w", "relative_h"]
        df_tutors = pd.DataFrame.from_records(tutor.to_dict() for tutor in tutors)
        print("df_tutors:", df_tutors)
        result = list(df_tutors[features].median())
        return result


    @staticmethod
    def impute_train_tutors(tutors):
        tutors_with_face, tutors_with_no_face = Preprocessor.split_by_face(tutors)
        median_features = Preprocessor.get_median_features(tutors_with_face)
        # print("median_features: ", median_features)
        for tutor in tutors_with_no_face:
            tutor.face_present, tutor.age, tutor.beauty_score, tutor.professional_score, tutor.gender, tutor.relative_x1, tutor.relative_y1, tutor.relative_w, tutor.relative_h = median_features

        return tutors_with_face + tutors_with_no_face
        # return tutors_with_face

    @staticmethod
    def impute_test_tutors(train_tutors, test_tutors):
        test_tutors_with_face, test_tutors_with_no_face = Preprocessor.split_by_face(test_tutors)
        train_tutors_with_face, train_tutors_with_no_face = Preprocessor.split_by_face(train_tutors)

        median_features = Preprocessor.get_median_features(train_tutors_with_face)
        # print("median_features: ", median_features)
        for tutor in test_tutors_with_no_face:
            tutor.face_present, tutor.age, tutor.beauty_score, tutor.professional_score, tutor.gender, tutor.relative_x1, tutor.relative_y1, tutor.relative_w, tutor.relative_h = median_features

        return test_tutors_with_face + test_tutors_with_no_face
        # return tutors_with_face




